package com.AceTheEvent.AceAppLLC;

import java.io.IOException;
import java.util.ArrayList;

import com.codename1.components.FloatingActionButton;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;

public class AboutForm extends Form {

	private Form backForm = null;
	private SideMenuButton menuButton = null;
	
	public AboutForm(Form backForm, Form logoutScreen) {
		super("About", new BorderLayout());
		
		this.backForm = backForm;
		
//		this.setSafeArea(true);
		this.setUIID("background");

		FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_ARROW_BACK);
		fab.addActionListener(e -> {
			menuButton.loadAllEvents();
		});
		fab.setUIID("Fab");
		fab.bindFabToContainer(this.getContentPane(), Component.LEFT, Component.BOTTOM);
		
		menuButton = new SideMenuButton(this, backForm, logoutScreen, fab, true);
		addFields();
	}
	
	private void addFields() {
		Label companyNameLabel = new Label("Ace the Event", "redLabelLightBlueBackground");
		Label versionLabel = new Label("Version " + SystemInfo.getVersion(), "UserProfileCenteredLabel");
		Label contactHeaderLabel = new Label("Contact E-mail", "redLabelLightBlueBackground");
		Label contactLabel = new Label("info@acetheevent.com", "UserProfileCenteredLabel");
		Label phoneHeaderLabel = new Label("Phone", "redLabelLightBlueBackground");
		Label phoneLabel = new Label("(856) 319-3141", "UserProfileCenteredLabel");
		Label addressHeaderLabel = new Label("Address", "redLabelLightBlueBackground");
		Label poLabel = new Label("P.O. Box 374", "UserProfileCenteredLabel");
//		Label addressLabel = new Label("966 Main St.", "UserProfileCenteredLabel");
		Label cityStZipLabel = new Label("Shiloh, NJ 08353", "UserProfileCenteredLabel");

		Container infoContainer = new Container(BoxLayout.y());
		addBlankLines(infoContainer, 5);
		infoContainer.add(companyNameLabel);
		infoContainer.add(versionLabel);
		addBlankLines(infoContainer, 3);
		infoContainer.add(contactHeaderLabel);
		infoContainer.add(contactLabel);
		addBlankLines(infoContainer, 3);
//		infoContainer.add(phoneHeaderLabel);
//		infoContainer.add(phoneLabel);
//		addBlankLines(infoContainer, 1);
		infoContainer.add(addressHeaderLabel);
		infoContainer.add(poLabel);
//		infoContainer.add(addressLabel);
		infoContainer.add(cityStZipLabel);
		infoContainer.setScrollableY(true);
		this.add(BorderLayout.CENTER, infoContainer);
	}
	
	private void addBlankLines(Container c, int numRows) {
		while (numRows-- > 0) {
			c.add(new Label(" ", "UserProfileCenteredLabel"));
		}
	}
}