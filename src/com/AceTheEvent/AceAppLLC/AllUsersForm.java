package com.AceTheEvent.AceAppLLC;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.codename1.components.SpanMultiButton;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;

public class AllUsersForm extends Form {

	public AllUsersForm(Form backForm) {
		super("", new BorderLayout());
//		this.setSafeArea(true);
		this.setUIID("background");
		
		Button backButton = new Button("Back");
		backButton.setTickerEnabled(false);
		backButton.addActionListener(event -> {
			backForm.show();
		});
		this.add(BorderLayout.NORTH, backButton);
		getUsers();
	}
	
	private Map<String, Object> getUsers() {
		BackendCall loginReq = new BackendCall("loadAllUsers", false);
		HashMap<String, Object> args = new HashMap();
		Map<String, Object> result = loginReq.makeCall(args);

		ArrayList users = (ArrayList) result.get("root");
		displayUsers(users);
		return result;
	}
	
	private void displayUsers(ArrayList users) {
		Container usersContainer = new Container(BoxLayout.y());
		usersContainer.setScrollableY(true);
		for (int i = 0; i < users.size(); i++) {
			UserInfo currUser = new UserInfo((LinkedHashMap)users.get(i));

			SpanMultiButton eventMultiButton = new SpanMultiButton();
			eventMultiButton.setTextLine1(currUser.getUserName());
			eventMultiButton.setTextLine2(currUser.getPhone());
			eventMultiButton.setTextLine3(currUser.getAddress());
			eventMultiButton.setUIIDLine1("EventMultiButtonHeader");
			eventMultiButton.setUIIDLine2("EventMultiButton");
			eventMultiButton.setUIIDLine3("EventMultiButton");
			eventMultiButton.setUIID("EventMultiButton");
			eventMultiButton.addActionListener(action -> {
				SingleUserForm singleUserForm = new SingleUserForm(this, currUser);
				singleUserForm.show();
			});
			usersContainer.add(eventMultiButton);
		}
		this.add(CENTER, usersContainer);
		this.revalidate();
	}
}