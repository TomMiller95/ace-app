package com.AceTheEvent.AceAppLLC;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Map;

import com.codename1.components.InfiniteProgress;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.FileSystemStorage;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.MultipartRequest;
import com.codename1.io.NetworkManager;
import com.codename1.push.Push;
import com.codename1.ui.Dialog;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Image;

public class BackendCall {

	private String path = ""; // This is the end point url EX: /login would be passed in for
								// http://localhost:8080/login

	private boolean setPost = false; // If this is a POST request

	public BackendCall(String path, boolean setPost) {
		this.path = path;
		this.setPost = setPost;
	}

	public void pictureUpload(Image img, String fileName) throws IOException {
		EncodedImage em = EncodedImage.createFromImage(img, false);
		byte[] data = em.getImageData();
		String filestack = getUrl();
		MultipartRequest request = new MultipartRequest() {
			protected void readResponse(InputStream input) throws IOException {
				JSONParser jp = new JSONParser();
				Map<String, Object> result = jp.parseJSON(new InputStreamReader(input, "UTF-8"));
				String url = (String) result.get("url");
				if (url == null) {
					return;
				}
			}
		};
		request.setUrl(filestack);
		request.addData("fileUpload", data, "image/jpeg");
		request.setFilename("fileUpload", fileName);
		NetworkManager.getInstance().addToQueue(request);
	}
	
	public Map<String, Object> makeMultiCall(Map<String, Object> args) {
		Dialog ip = new InfiniteProgress().showInifiniteBlocking();
		try {
			MultipartRequest r = new MultipartRequest();
			r.setPost(setPost);
			r.setUrl(getUrl());
			r.addArgument("authToken", SystemInfo.authToken);
			r.addArgument("pretty", "0");
			r.addArgument("action", "search_listings");
			r.addArgument("encoding", "json");
			for (Map.Entry<String, Object> entry : args.entrySet()) {
				String key = entry.getKey();
				Object value = entry.getValue();
				r.addArgument(key, value + "");
			}
			NetworkManager.getInstance().addToQueueAndWait(r);
			Map<String, Object> result = new JSONParser()
					.parseJSON(new InputStreamReader(new ByteArrayInputStream(r.getResponseData()), "UTF-8"));
			ip.dispose();
			if (result.isEmpty()) {
				return null;
			}
			return result;
		} catch (Exception err) {
			ip.dispose();
			Log.e(err);
		}
		return null; // Probably shouldn't make it here
	}

	// TODO Probably add a timeout option?
	public Map<String, Object> makeCall(Map<String, Object> args) {
		Dialog ip = new InfiniteProgress().showInifiniteBlocking();
		try {
			ConnectionRequest r = new ConnectionRequest();
			r.setPost(setPost);
			r.setUrl(getUrl());
			r.addArgument("authToken", SystemInfo.authToken);
			r.addArgument("pretty", "0");
			r.addArgument("action", "search_listings");
			r.addArgument("encoding", "json");
			for (Map.Entry<String, Object> entry : args.entrySet()) {
				String key = entry.getKey();
				Object value = entry.getValue();
				r.addArgument(key, value + "");
			}
			NetworkManager.getInstance().addToQueueAndWait(r);
			Map<String, Object> result = new JSONParser()
					.parseJSON(new InputStreamReader(new ByteArrayInputStream(r.getResponseData()), "UTF-8"));
			ip.dispose();
			if (result.isEmpty()) {
				return null;
			}
			if (result.containsKey("MAINTENANCE_MODE")) {
				launchMaintenanceDialog();
				return null;
			}
			return result;
		} catch (Exception err) {
			ip.dispose();
			Log.e(err);
		}
		return null; // Probably shouldn't make it here
	}

	private void launchMaintenanceDialog() {
		
	}
	
	private String getUrl() {
//		return "http://localhost:8080/" + path; // Testing local
		return "https://ace-the-event.cloud/" + path; // Production
	}
}