package com.AceTheEvent.AceAppLLC;

import java.util.Map;

public class CheckInRecord {
	
	private double id = -1;
	private String eventId = "";
	private String userId = "";
	private String checkInTime = "";
	private String checkOutTime = "";

	public CheckInRecord(Map<String, Object> data) {
		setInfo(data);
	}

	private void setInfo(Map<String, Object> data) {
		for (Map.Entry<String, Object> entry : data.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();
			switch (key) {
			case "id":
				id = (double) value;
				break;
			case "eventId":
				eventId = (String) value;
				break;
			case "userId":
				userId = (String) value;
				break;
			case "checkInTime":
				checkInTime = (String) value;
				break;
			case "checkOutTime":
				checkOutTime = (String) value;
				break;
			default:
				System.out.println("This was a bad key: " + key);
				break;
			}
		}
	}
	
	public double getId() {
		return id;
	}

	public String getEventId() {
		return eventId;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public String getCheckInTime() {
		return checkInTime;
	}
	
	public String getCheckOutTime() {
		return checkOutTime;
	}
}