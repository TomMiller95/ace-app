package com.AceTheEvent.AceAppLLC;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import com.codename1.capture.Capture;
import com.codename1.components.ImageViewer;
import com.codename1.io.BufferedOutputStream;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.File;
import com.codename1.io.FileSystemStorage;
import com.codename1.io.JSONParser;
import com.codename1.io.MultipartRequest;
import com.codename1.io.NetworkManager;
import com.codename1.io.Util;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Image;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.list.DefaultListModel;
import com.codename1.ui.util.ImageIO;

public class ContactPhotoDialog extends Dialog {

	public ContactPhotoDialog() {
		super("Please select a contact photo to continue");
		this.setUIID("HeaderBackground");
		this.setDialogUIID("HeaderBackground");
		this.getTitleComponent().setUIID("Label");

		Button postButton = new Button("Select Photo");
		postButton.setTickerEnabled(false);
		postButton.setUIID("FatButton");
		postButton.addActionListener(action -> {
//			String picture = "";// Capture.capturePhoto();
			Display.getInstance().openGallery((e) -> {
				if (e != null && e.getSource() != null) {
					String filePath = (String) e.getSource();
					Image img = null;
					try {
						img = Image.createImage(filePath);
//						BackendCall uploadPhotoReq = new BackendCall("sendImage", true);
//						HashMap<String, Object> args = new HashMap();

						FileSystemStorage fss = FileSystemStorage.getInstance();
//				        boolean isJpeg = Image.isJPEG(fss.openInputStream(filePath));
						boolean isPNG = Image.isPNG(fss.openInputStream(filePath));

						String fileType = isPNG ? ".png" : ".jpeg";

						////////////////
						uploadImageToS3WithChatGPTsHelp(filePath, isPNG ? SystemInfo.userInfo.getPresignedPutUrlPNG()
								: SystemInfo.userInfo.getPresignedPutUrlJPEG());
//						uploadToS3(isPNG ? SystemInfo.userInfo.getPresignedPutUrlPNG()
//								: SystemInfo.userInfo.getPresignedPutUrlJPEG(), filePath, isPNG, img);
						////////////////

//						uploadPhotoReq.pictureUpload(img, SystemInfo.userInfo.getId() + "_"
//								+ SystemInfo.userInfo.getUserName().toUpperCase() + fileType);
//						setS3GetURL(fileType); // TODO Don't do this if the above method fails
						dispose();
					} catch (Exception err) {
						err.printStackTrace();
					}
				}
			}, Display.GALLERY_IMAGE);
		});

		Container buttonsContainer = new Container(BoxLayout.y());
		buttonsContainer.setUIID("HeaderBackground");
		buttonsContainer.add(postButton);

		this.add(buttonsContainer);
		this.setDisposeWhenPointerOutOfBounds(false);
		this.show();
	}

	public void uploadImageToS3WithChatGPTsHelp(String filePath, String presignedUrl) {
	    try {
	    	System.out.println(presignedUrl);
	        MultipartRequest request = new MultipartRequest();
	        request.setUrl(presignedUrl);
	        request.addData("file", FileSystemStorage.getInstance().openInputStream(filePath), FileSystemStorage.getInstance().getLength(filePath), "image/png");
	        NetworkManager.getInstance().addToQueue(request);
	    } catch (IOException ex) {
	        ex.printStackTrace();
	    }
	}


	private void uploadToS3(String url, String filePath, boolean isPNG, Image img)
			throws UnsupportedEncodingException, IOException {

		EncodedImage em = EncodedImage.createFromImage(img, false); // TODO false should be checked here.
		byte[] data = em.getImageData();

		MultipartRequest request = new MultipartRequest();
		request.setPost(true);
		request.setUrl(url);
//		request.setUrl("https://s3.amazonaws.com/ace-app-images");
//		request.addArgument("x-api-key", "AKIAQ7I2VP5GSYLIR2PY");
//		request.addArgument("x-api-secret", "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");

		request.addResponseListener(e -> {
			String response = new String((byte[])e.getMetaData());
			System.out.println(response);
		});
		
		request.addData("file", data, "imageName.jpg");
		
		NetworkManager.getInstance().addToQueue(request);

	/////////////////////////////

//		EncodedImage em = EncodedImage.createFromImage(img, false); // TODO false should be checked here.
//		byte[] data = em.getImageData();
//		
//		ConnectionRequest r = new ConnectionRequest();
//		r.setUrl(url);
//		r.setHttpMethod("PUT");
//		r.addArgument("file", data);
//		r.addArgument("body", data);
//		r.addRequestHeader("Content-Type", "image/jpg");
//		r.addArgument("fileUpload", data);
//
//		NetworkManager.getInstance().addToQueueAndWait(r);
//		Map<String, Object> result = new JSONParser()
//				.parseJSON(new InputStreamReader(new ByteArrayInputStream(r.getResponseData()), "UTF-8"));
//		System.out.println(result);

	/////////////////////////////

//		MultipartRequest request = new MultipartRequest() {
//			protected void readResponse(InputStream input) throws IOException {
//				JSONParser jp = new JSONParser();
//				Map<String, Object> result = jp.parseJSON(new InputStreamReader(input, "UTF-8"));
//				String url = (String) result.get("url");
//				dispose();
//				if (url == null) {
//					return;
//				}
//			}
//		};
//		
//		request.setUrl(
//				isPNG ? SystemInfo.userInfo.getPresignedPutUrlPNG() : SystemInfo.userInfo.getPresignedPutUrlJPEG());
//		try {
//			request.setHttpMethod("PUT");
//			request.setReadResponseForErrors(true);
//			request.addData("file", filePath, "image/" + (isPNG ? "png" : "jpeg"));
//			NetworkManager.getInstance().addToQueue(request);
//		} catch (Exception err) {
//			err.printStackTrace();
//		}
	}

	private void setS3GetURL(String fileType) {
		BackendCall loginReq = new BackendCall("setS3GetURL", true);
		HashMap<String, Object> args = new HashMap();
		args.put("userId", SystemInfo.userInfo.getId());
		args.put("fileType", fileType);
		Map<String, Object> result = loginReq.makeCall(args);
		SystemInfo.userInfo = new UserInfo(result);
	}

}
