package com.AceTheEvent.AceAppLLC;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.codename1.components.FloatingActionButton;
import com.codename1.components.SpanMultiButton;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.spinner.Picker;

public class EventByDateForm extends Form {

	Form logoutScreen = null;
	private SideMenuButton menuButton = null;

	public EventByDateForm(Form backForm, Form logoutScreen) {
		super("Search Events By Date", new BorderLayout());
		this.setUIID("background");
		this.logoutScreen = logoutScreen;

		menuButton = new SideMenuButton(this, backForm, logoutScreen, null, true);
		getEvents("2022-11-15");

		FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_ARROW_BACK);
		fab.addActionListener(e -> {
			backForm.show();
		});
		fab.setUIID("Fab");
		fab.bindFabToContainer(this.getContentPane(), Component.LEFT, Component.BOTTOM);

		Container filterContainer = new Container(BoxLayout.x());
		Picker datePicker = new Picker();
		datePicker.setType(Display.PICKER_TYPE_DATE);
		datePicker.addActionListener(action -> {
			String formattedDate = SystemInfo.formatDate(datePicker.getText());
			this.removeAll();
			getEvents(formattedDate);
			this.add(TOP, filterContainer);
			this.revalidate();
		});
		
		datePicker.setUIID("PickerButton");

		filterContainer.add(datePicker);
		this.add(TOP, filterContainer);
		this.revalidate();
	}

	private Map<String, Object> getEvents(String date) {
		BackendCall req = new BackendCall("loadEventsByDate", false);
		HashMap<String, Object> args = new HashMap();
		args.put("date", date);
		Map<String, Object> result = req.makeCall(args);

		ArrayList events = (ArrayList) result.get("root");
		displayEvents(events);
		return result;
	}

	private void displayEvents(ArrayList events) {
		Container eventsContainer = new Container(BoxLayout.y());
		eventsContainer.setScrollableY(true);
		for (int i = 0; i < events.size(); i++) {
			EventInfo currEvent = new EventInfo((LinkedHashMap) events.get(i));

			SpanMultiButton eventMultiButton = new SpanMultiButton();
			eventMultiButton.setTextLine1(currEvent.getFormattedEventDate());
			eventMultiButton.setTextLine2(currEvent.getOsmStartTime() + " to " + currEvent.getOsmEndTime());
			eventMultiButton.setTextLine3(currEvent.getAddress());
			eventMultiButton
					.setTextLine4(currEvent.getCity() + ", " + currEvent.getState() + ", " + currEvent.getZip());
			eventMultiButton.setUIIDLine1("GoldLabelLightBackground");
			eventMultiButton.setUIIDLine2("EventMultiButtonHeader2");
			eventMultiButton.setUIIDLine3("EventMultiButton");
			eventMultiButton.setUIIDLine4("EventMultiButtonLabel2");
			eventMultiButton.setUIID("EventMultiButton");

			eventMultiButton.addActionListener(action -> {
				SingleEventForm singleEventForm = new SingleEventForm(this, currEvent, logoutScreen);
				singleEventForm.show();
			});
			eventsContainer.add(eventMultiButton);
		}
		this.add(CENTER, eventsContainer);
		this.revalidate();
	}

}
