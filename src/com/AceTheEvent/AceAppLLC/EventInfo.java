package com.AceTheEvent.AceAppLLC;

import java.util.ArrayList;
import java.util.Map;

import com.codename1.ui.Label;
import com.codename1.ui.TextField;

public class EventInfo {
	
	private double id = -1;
	private String osmStartTime = "";
	private String osmEndTime = "";
	private String ossStartTime = "";
	private String ossEndTime = "";
	private String backupStartTime = "";
	private String backupEndTime = "";
	private String osmtStartTime = "";
	private String osmtEndTime = "";
	private String scriberStartTime = "";
	private String scriberEndTime = "";
	private String spotterStartTime = "";
	private String spotterEndTime = "";
	private String notes = "";
	private String city = "";
	private String state = "";
	private String zip = "";
	private String address = "";
	private String description = "";
	private String eventDate = "";
	private String numOssNeeded = "";
	private String numOsmNeeded = "";
	private String numAosmNeeded = "";
	private String numBackupsNeeded = "";
	private String numOsmtNeeded = "";
	private String numScriberNeeded = "";
	private String numSpotterNeeded = "";
	private String venue = "";
	private String organization = "";
	private String ossAmount = "";
	private String publicDesc = "";
	private String managerNote = "";
	private ArrayList users = null;

	public EventInfo(Map<String, Object> data) {
		setInfo(data);
	}

	private void setInfo(Map<String, Object> data) {
		for (Map.Entry<String, Object> entry : data.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();
			switch (key) {
			case "id":
				id = (double)value;
				break;
			case "osmStartTime":
				osmStartTime = (String)value;
				break;
			case "osmEndTime":
				osmEndTime = (String)value;
				break;
			case "ossStartTime":
				ossStartTime = (String)value;
				break;
			case "ossEndTime":
				ossEndTime = (String)value;
				break;
			case "backupStartTime":
				backupStartTime = (String)value;
				break;
			case "backupEndTime":
				backupEndTime = (String)value;
				break;
			case "osmtStartTime":
				osmtStartTime = (String)value;
				break;
			case "osmtEndTime":
				osmtEndTime = (String)value;
				break;
			case "spotterStartTime":
				spotterStartTime = (String)value;
				break;
			case "spotterEndTime":
				spotterEndTime = (String)value;
				break;
			case "scriberStartTime":
				scriberStartTime = (String)value;
				break;
			case "scriberEndTime":
				scriberEndTime = (String)value;
				break;
			case "notes":
				notes = (String)value;
				break;
			case "city":
				city = (String)value;
				break;
			case "state":
				state = (String)value;
				break;
			case "zip":
				zip = (String)value;
				break;
			case "address":
				address = (String)value;
				break;
			case "eventDesc":
				description = (String)value;
				break;
			case "eventDate":
				eventDate = (String)value;
				break;
			case "users":
				users = (ArrayList)value;
				break;
			case "numOssNeeded":
				numOssNeeded = (String)value;
				break;
			case "numOsmNeeded":
				numOsmNeeded = (String)value;
				break;
			case "numAosmNeeded":
				numAosmNeeded = (String)value;
				break;
			case "numBackupsNeeded":
				numBackupsNeeded = (String)value;
				break;
			case "numOsmtNeeded":
				numOsmtNeeded = (String)value;
				break;
			case "numScriberNeeded":
				numScriberNeeded = (String)value;
				break;
			case "numSpotterNeeded":
				numSpotterNeeded = (String)value;
				break;
			case "venue":
				venue = (String)value;
				break;
			case "organization":
				organization = (String)value;
				break;
			case "ossAmount":
				ossAmount = (String)value;
				break;
			case "publicDesc":
				publicDesc = (String)value;
				break;
			case "managerNote":
				managerNote = (String)value;
				break;
			default:
				System.out.println("This was a bad key: " + key);
				break;
			}
		}
	}
	
	public String getNumOssNeeded() {
		return numOssNeeded;
	}
	
	public String getNumOsmNeeded() {
		return numOsmNeeded;
	}

	public String getNumAosmNeeded() {
		return numAosmNeeded;
	}

	public String getNumBackupsNeeded() {
		return numBackupsNeeded;
	}

	public String getNumOsmtNeeded() {
		return numOsmtNeeded;
	}

	public String getNumSpotterNeeded() {
		return numSpotterNeeded;
	}

	public String getNumScriberNeeded() {
		return numScriberNeeded;
	}

	public int getId() {
		return (int)id;
	}
	
	public String getOsmStartTime() {
		return osmStartTime;
	}
	
	public String getOssStartTime() {
		return ossStartTime;
	}

	public void setOssStartTime(String ossStartTime) {
		this.ossStartTime = ossStartTime;
	}

	public String getOssEndTime() {
		return ossEndTime;
	}

	public void setOssEndTime(String ossEndTime) {
		this.ossEndTime = ossEndTime;
	}

	public String getBackupStartTime() {
		return backupStartTime;
	}

	public void setBackupStartTime(String backupStartTime) {
		this.backupStartTime = backupStartTime;
	}

	public String getBackupEndTime() {
		return backupEndTime;
	}

	public void setBackupEndTime(String backupEndTime) {
		this.backupEndTime = backupEndTime;
	}
	
	public String getOsmtStartTime() {
		return osmtStartTime;
	}

	public void setOsmtStartTime(String osmtStartTime) {
		this.osmtStartTime = osmtStartTime;
	}

	public String getOsmtEndTime() {
		return osmtEndTime;
	}

	public void setOsmtEndTime(String osmtEndTime) {
		this.osmtEndTime = osmtEndTime;
	}
	
	public String getSpotterStartTime() {
		return spotterStartTime;
	}

	public void setSpotterStartTime(String spotterStartTime) {
		this.spotterStartTime = spotterStartTime;
	}

	public String getSpotterEndTime() {
		return spotterEndTime;
	}

	public void setSpotterEndTime(String spotterEndTime) {
		this.spotterEndTime = spotterEndTime;
	}
	
	public String getScriberStartTime() {
		return scriberStartTime;
	}

	public void setScriberStartTime(String scriberStartTime) {
		this.scriberStartTime = scriberStartTime;
	}

	public String getScriberEndTime() {
		return scriberEndTime;
	}

	public void setScriberEndTime(String scriberEndTime) {
		this.scriberEndTime = scriberEndTime;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getOsmEndTime() {
		return osmEndTime;
	}
	
	public String getCity() {
		return city;
	}
	
	public String getState() {
		return state;
	}
	
	public String getZip() {
		return zip;
	}
	
	public String getAddress() {
		return address;
	}

	public String getDescription() {
		return description;
	}

	public String getEventDate() {
		return eventDate;
	}


	public String getVenue() {
		return venue;
	}

	public void setVenua(String venue) {
		this.venue = venue;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}
	
	public String getOssAmount() {
		return ossAmount;
	}
	
	public void setOssAmount(String ossAmount) {
		this.ossAmount = ossAmount;
	}

	public String getPublicDesc() {
		return publicDesc;
	}

	public void setPublicDesc(String publicDesc) {
		this.publicDesc = publicDesc;
	}
	
	public String getManagerNote() {
		return managerNote;
	}
	
	public void setManagerNote(String managerNote) {
		this.managerNote = managerNote;
	}

	public String getFormattedEventDate() {
		// TODO make this cleaner
		if (eventDate.trim().length() != 10) {
			return "";
		}
		return eventDate.substring(5,7) + "/" + eventDate.substring(8,10) + "/" + eventDate.substring(0,4);
	}
	
	public ArrayList getUsers() {
		return users;
	}
}
