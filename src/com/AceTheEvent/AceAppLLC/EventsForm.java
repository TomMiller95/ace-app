package com.AceTheEvent.AceAppLLC;

import java.io.IOException;
import java.util.ArrayList;
import com.codename1.ui.Form;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.util.Resources;

public class EventsForm extends Form {

	private Resources theme = null;
	private ArrayList<String> suitList = new ArrayList<>();
	private SideMenuButton menuButton = null;
	private Form logoutScreen = null;
	private String singleEventId = ""; // This is for notifications to load an event
	
	public EventsForm(Form backForm) {
		super("Today's Event!", new BorderLayout());

//		if (SystemInfo.userInfo.getContactPhotoKey().trim().equals("")) {
//			ContactPhotoDialog c = new ContactPhotoDialog();
//		}
		
		logoutScreen = backForm;

		this.setUIID("backgroundWithImageNEW");

		suitList.add("/Diamond.png");
		suitList.add("/Club.png");
		suitList.add("/Heart.png");
		suitList.add("/Spade.png");

		try {
			theme = Resources.openLayered("/theme");
		} catch (IOException e) {
			System.out.println(e);
		}
		menuButton = new SideMenuButton(this, backForm, logoutScreen, null, true);
	}

	public void setLastLookUp(String lookUp) {
		menuButton.setLastLoaded(lookUp);
	}

//	public void openEventByNotification(String id) {
//		// When a user enters the app from a notification, this should load the event
//		// associated with the id
//		if (id.trim().length() > 0) {
//			menuButton.getSpecificEvent(id);
//		}
//	}
	
	public void setSingleEventId(String id) {
		singleEventId = id;
	}

	@Override
	public void show() {
		boolean showForm = true;
		menuButton.clearForm();
		switch (menuButton.getLastLoaded()) {
		case "allEvents":
			menuButton.getAllEvents();
			break;
		case "filteredEvents":
			menuButton.getAllFilteredEvents();
			break;
		case "usersInterestedEvents":
			menuButton.getUsersInterestedEvents();
			break;
		case "usersBookedEvents":
			menuButton.getUsersBookedEvents();
			break;
		case"usersLast30Events":
			menuButton.getUsersLastThirtyDaysEvents();
			break;
		case "unbooked":
			menuButton.getUnbookedEvents();
			break;
		case "todaysEvents":
			menuButton.getTodaysEvents();
			break;
		case "notificationEvent":
			menuButton.getSpecificEvent(singleEventId);
			setLastLookUp("allEvents");
			showForm = false;
			break;
		default:
			System.out.println("Tried to refresh events but had this case: " + menuButton.getLastLoaded());
		}
		if (showForm) {
			super.show();
		}
	}
}