package com.AceTheEvent.AceAppLLC;

import com.codename1.components.FloatingActionButton;
import com.codename1.components.ImageViewer;
import com.codename1.ui.Component;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.layouts.BorderLayout;

public class ImageForm extends Form {

	private SideMenuButton menuButton = null;
	
	public ImageForm(String contactName, Image img, Form backForm, Form logoutScreen) {
		super(contactName, new BorderLayout());
		this.setUIID("background");
		
		FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_ARROW_BACK);
		fab.addActionListener(e -> {
			backForm.show();
		});
		fab.setUIID("Fab");
		fab.bindFabToContainer(this.getContentPane(), Component.LEFT, Component.BOTTOM);
		
		menuButton = new SideMenuButton(this, backForm, logoutScreen, fab, true);
		
		ImageViewer iv = new ImageViewer(img);
		iv.setUIID("background");
		this.add(BorderLayout.CENTER, iv);
	}
}
