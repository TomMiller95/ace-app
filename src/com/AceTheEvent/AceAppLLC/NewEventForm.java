package com.AceTheEvent.AceAppLLC;

import java.util.HashMap;
import java.util.Map;

import com.codename1.ui.Button;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.spinner.Picker;

public class NewEventForm extends Form {


	public NewEventForm(Form backForm) {
		super("Add New Event", BoxLayout.y());
//		this.setSafeArea(true);

		Button backButton = new Button("Back");
		backButton.setTickerEnabled(false);
		backButton.addActionListener(event -> {
			backForm.show();
		});
		this.add(backButton);

		Label osmStartTimeLabel = new Label("Osm Start Time");
		TextField osmStartTimeField = new TextField();
		Label osmEndTimeLabel = new Label("Osm End Time");
		TextField osmEndTimeField = new TextField();
		Label ossStartTimeLabel = new Label("Oss Start Time");
		TextField ossStartTimeField = new TextField();
		Label ossEndTimeLabel = new Label("Oss End Time");
		TextField ossEndTimeField = new TextField();
		Label backupStartTimeLabel = new Label("Backup Start Time");
		TextField backupStartTimeField = new TextField();
		Label backupEndTimeLabel = new Label("Backup End Time");
		TextField backupEndTimeField = new TextField();
		Label notesLabel = new Label("Notes");
		TextField notesField = new TextField();
		Label cityLabel = new Label("City");
		TextField cityField = new TextField();
		Label stateLabel = new Label("State");
		Picker stateField = new Picker();
		stateField.setStrings(getStates());
		stateField.setSelectedStringIndex(0);
		Label zipLabel = new Label("Zip");
		TextField zipField = new TextField("", "", 4, TextArea.NUMERIC);
		Label addressLabel = new Label("Address");
		TextField addressField = new TextField();
		Label descLabel = new Label("Description");
		TextField descField = new TextField();
		Label dateLabel = new Label("Date");
		TextField dateField = new TextField();

		Label venueLabel = new Label("Venue");
		TextField venueField = new TextField();
		Label organizationLabel = new Label("Organization");
		TextField organizationField = new TextField();
		Label publicDescLabel = new Label("Public Desc");
		TextField publicDescField = new TextField();
		Label managerNoteLabel = new Label("Manager Note");
		TextField managerNoteField = new TextField();

		Label dnumOssLabel = new Label("Number of OSS");
		TextField numOssField = new TextField();
		Label numOsmLabel = new Label("Number of OSM");
		TextField numOsmField = new TextField();
		Label numAosmLabel = new Label("Number of AOSM");
		TextField numAosmField = new TextField();
		Label numBackupsLabel = new Label("Number of Backups");
		TextField numBackupsField = new TextField();
		this.add(osmStartTimeLabel);
		this.add(osmStartTimeField);
		this.add(osmEndTimeLabel);
		this.add(osmEndTimeField);
		this.add(ossStartTimeLabel);
		this.add(ossStartTimeField);
		this.add(ossEndTimeLabel);
		this.add(ossEndTimeField);
		this.add(backupStartTimeLabel);
		this.add(backupStartTimeField);
		this.add(backupEndTimeLabel);
		this.add(backupEndTimeField);
		this.add(notesLabel);
		this.add(notesField);
		this.add(addressLabel);
		this.add(addressField);
		this.add(cityLabel);
		this.add(cityField);
		this.add(stateLabel);
		this.add(stateField);
		this.add(zipLabel);
		this.add(zipField);
		this.add(descLabel);
		this.add(descField);
		this.add(dateLabel);
		this.add(dateField);

		this.add(organizationLabel);
		this.add(organizationField);
		this.add(venueLabel);
		this.add(venueField);
		this.add(publicDescLabel);
		this.add(publicDescField);
		this.add(managerNoteLabel);
		this.add(managerNoteField);

		this.add(dnumOssLabel);
		this.add(numOssField);
		this.add(numOsmLabel);
		this.add(numOsmField);
		this.add(numAosmLabel);
		this.add(numAosmField);
		this.add(numBackupsLabel);
		this.add(numBackupsField);

		Button addEventButton = new Button("Add New Event");
		addEventButton.setTickerEnabled(false);
		this.add(addEventButton);
		addEventButton.addActionListener(event -> {
			addNewEvent(osmStartTimeField.getText(), osmEndTimeField.getText(), ossStartTimeField.getText(),
					ossEndTimeField.getText(), backupStartTimeField.getText(), backupEndTimeField.getText(),
					notesField.getText(), cityField.getText(), stateField.getText(), zipField.getText(),
					addressField.getText(), descField.getText(), dateField.getText(), numOssField.getText(),numOsmField.getText(),
					numAosmField.getText(), numBackupsField.getText(), organizationField.getText(), venueField.getText(), publicDescField.getText(), managerNoteField.getText());
		});
	}

	private Map<String, Object> addNewEvent(String osmStartTime, String osmEndTime, String ossStartTime,
			String ossEndTime, String backupStartTime, String backupEndTime, String notes, String city, String state,
			String zip, String address, String eventDesc, String eventDate, String numOss, String numOsm, String numAosm,
			String numBackups, String organization, String venue, String publicDesc, String managerNote) {
		BackendCall loginReq = new BackendCall("addEvent", false);
		HashMap<String, Object> args = new HashMap();
		args.put("osmStartTime", osmStartTime);
		args.put("osmEndTime", osmEndTime);
		args.put("ossStartTime", ossStartTime);
		args.put("ossEndTime", ossEndTime);
		args.put("backupStartTime", backupStartTime);
		args.put("backupEndTime", backupEndTime);
		args.put("notes", notes);
		args.put("city", city);
		args.put("state", state);
		args.put("zip", zip);
		args.put("address", address);
		args.put("eventDesc", eventDesc);
		args.put("eventDate", eventDate);
		args.put("numOss", numOss);
		args.put("numOsm", numOsm);
		args.put("numAosm", numAosm);
		args.put("numBackups", numBackups);
		args.put("organization", organization);
		args.put("venue", venue);
		args.put("publicDesc", publicDesc);
		args.put("managerNote", managerNote);
		Map<String, Object> result = loginReq.makeCall(args);
		this.repaint();
		this.refreshTheme();
		return null; // TODO Make this load something I guess
	}

	private String[] getStates() {
		String[] states = { "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA",
				"KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "NE", "NV", "NH", "NJ", "NM", "NY", "NC",
				"ND", "KS", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI",
				"WY" };
		return states;
	}
}
