package com.AceTheEvent.AceAppLLC;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.spinner.Picker;

public class NewUserForm extends Form {

	public NewUserForm(Form backForm) {
		super("Add New User", BoxLayout.y());
//		this.setSafeArea(true);

		Button backButton = new Button("Back");
		backButton.setTickerEnabled(false);
		backButton.addActionListener(event -> {
			backForm.show();
		});
		this.add(backButton);


		Label userNameLabel = new Label("User Name");
		TextField userNameField = new TextField("", "", 20, TextField.USERNAME);
		
		Label firstNameLabel = new Label("First Name");
		TextField firstNameField = new TextField();
		
		Label lastNameLabel = new Label("Last Name");
		TextField lastNameField = new TextField();
		
		Label passwordLabel = new Label("Password");
		TextField passwordField = new TextField("", "", 20, TextField.PASSWORD);
		
		Label cityLabel = new Label("City");
		TextField cityField = new TextField();
		
		Label stateLabel = new Label("State");
		Picker stateField = new Picker();
		stateField.setStrings(getStates());
		stateField.setSelectedStringIndex(0);
		
		Label zipLabel = new Label("Zip");
		TextField zipField = new TextField("", "", 4, TextArea.NUMERIC);
		
		Label addressLabel = new Label("Address");
		TextField addressField = new TextField();
		
		Label address2Label = new Label("Address 2");
		TextField address2Field = new TextField();
		
		Label phoneLabel = new Label("Phone Number");
		TextField phoneField = new TextField("", "", 20, TextArea.PHONENUMBER);
		
		Label emailLabel = new Label("Email Address");
		TextField emailField = new TextField("", "", 35, TextArea.EMAILADDR);

		this.add(userNameLabel);
		this.add(userNameField);
		this.add(firstNameLabel);
		this.add(firstNameField);
		this.add(lastNameLabel);
		this.add(lastNameField);
		this.add(passwordLabel);
		this.add(passwordField);
		this.add(addressLabel);
		this.add(addressField);
		this.add(address2Label);
		this.add(address2Field);
		this.add(cityLabel);
		this.add(cityField);
		this.add(stateLabel);
		this.add(stateField);
		this.add(zipLabel);
		this.add(zipField);
		this.add(phoneLabel);
		this.add(phoneField);
		this.add(emailLabel);
		this.add(emailField);

		Button addNewUserButton = new Button("Add New User");
		addNewUserButton.setTickerEnabled(false);
		this.add(addNewUserButton);
		addNewUserButton.addActionListener(event -> {
			addNewUser(userNameField.getText(), firstNameField.getText(), lastNameField.getText(), passwordField.getText(), cityField.getText(), stateField.getText(),
					zipField.getText(), addressField.getText(), address2Field.getText(), phoneField.getText());
		});
	}

	private Map<String, Object> addNewUser(String userName, String firstName, String lastName, String pass, String city, String state, String zip,
			String address, String address2, String number) {
		try {
			BackendCall loginReq = new BackendCall("addUser", false);
			HashMap<String, Object> args = new HashMap();
			args.put("userName", userName);
			args.put("firstName", firstName);
			args.put("lastName", lastName);
			args.put("password", pass);
			args.put("city", city);
			args.put("state", state);
			args.put("zip", zip);
			args.put("address", address);
			args.put("address2", address2);
			args.put("phone", number);
			args.put("accessLevel", "C");
			Map<String, Object> result = loginReq.makeCall(args);

			AllUsersForm allUsersForm = new AllUsersForm(this);
			allUsersForm.show();
			
		} catch (Exception err) {
			Log.e(err);
		}
		return null; // Probably shouldn't make it here
	}

	private String[] getStates() {
		String[] states = { "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA",
				"KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "NE", "NV", "NH", "NJ", "NM", "NY", "NC",
				"ND", "KS", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI",
				"WY" };
		return states;
	}
}
