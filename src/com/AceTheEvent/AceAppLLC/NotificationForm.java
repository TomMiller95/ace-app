package com.AceTheEvent.AceAppLLC;

import java.util.HashMap;
import java.util.Map;

import com.codename1.components.FloatingActionButton;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.spinner.Picker;

public class NotificationForm extends Form {

	private SideMenuButton menuButton = null;
	
	public NotificationForm(Form backForm, Form logoutScreen) {
		super("Send Notification", new BorderLayout());

		this.setUIID("background");

		FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_ARROW_BACK);
		fab.addActionListener(e -> {
			menuButton.loadAllEvents();
		});
		fab.setUIID("Fab");
		fab.bindFabToContainer(this.getContentPane(), Component.LEFT, Component.BOTTOM);

		menuButton = new SideMenuButton(this, backForm, logoutScreen, fab, true);
		
		addFields();
	}
	
	private void addFields() {

		Container fields = new Container(BoxLayout.y());
		fields.setUIID("background");
		
		addBlankLines(fields, 2);
		
		Label statesLabel = new Label("State to Notify", "UserProfileCenteredLabel");
		fields.add(statesLabel);
		
		Picker stateField = new Picker();
		stateField.setStrings(getStates());
		fields.add(stateField);

		addBlankLines(fields, 1);
		
		Label notificationLabel = new Label("Text to Send", "UserProfileCenteredLabel");
		fields.add(notificationLabel);
		
		TextArea notificationField = new TextArea();
		notificationField.setEditable(true);
//		notificationField.setUIID("WhiteLabelCentered");
		notificationField.setRows(5);
		fields.add(notificationField);
		
		addBlankLines(fields, 1);
		
		Button sendButton = new Button("Send Notification", "FatButtonLight");
		sendButton.setTickerEnabled(false);
		sendButton.addActionListener(action -> {
			sendNotification(stateField.getText().trim(), notificationField.getText().trim());
		});
		fields.add(sendButton);

		this.add(BorderLayout.CENTER, fields);
	}
	
	private void sendNotification(String state, String msg) {
		BackendCall loginReq = new BackendCall("sendNotification", false);
		HashMap<String, Object> args = new HashMap();
		args.put("state", state);
		args.put("notification", msg);
		Map<String, Object> result = loginReq.makeCall(args);
		System.out.println(result);
	}
	
	private void addBlankLines(Container c, int numRows) {
		while (numRows-- > 0) {
			c.add(new Label(" ", "UserProfileCenteredLabel"));
		}
	}
	
	private String[] getStates() {
		String[] abbrevs = { "Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut",
				"Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas",
				"Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi",
				"Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Jersey", "New Mexico", "New York",
				"North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island",
				"South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington",
				"West Virginia", "Wisconsin", "Wyoming" };
		return abbrevs;
	}
}
