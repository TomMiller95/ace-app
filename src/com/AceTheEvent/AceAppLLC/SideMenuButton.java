package com.AceTheEvent.AceAppLLC;

import java.util.ArrayList;

import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.codename1.components.FloatingActionButton;
import com.codename1.components.SpanMultiButton;
import com.codename1.io.Preferences;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.spinner.Picker;

public class SideMenuButton {

	private Form form = null;
	private Form logoutScreen = null;
	private String lastLoaded = "todaysEvents";
	private FloatingActionButton fab = null;
	private String updatedSendEventsString = "";
	private Toolbar tb = null;
//	private boolean addedNotifications = false;
	private Command notificationCommand = null;

	TextField filterCityField = new TextField("", "City");
	Picker stateFilterField = new Picker();
	Picker dateFilterPicker = new Picker();

	public SideMenuButton(Form form, Form backForm, Form logoutScreen, FloatingActionButton fab, boolean showToolbar) {
		this.form = form;
		this.logoutScreen = logoutScreen;
		this.fab = fab;

		Toolbar.setEnableSideMenuSwipe(false);
		tb = form.getToolbar();

		tb.setUIID("TouchCommandNEW");
		Image icon = null;
		try {
//			icon = Image.createImage("/AceLogo.png");
			icon = Image.createImage("/AceLogoNew.png");
		} catch (Exception e) {
			System.out.println(e);
		}

		if (showToolbar) {
			Container topBar = BorderLayout.center(new Label(icon));
//		topBar.add(BorderLayout.SOUTH, new Label("Menu", "SidemenuTagline"));
//			tb.addComponentToSideMenu(new Label(" "));

			tb.addComponentToSideMenu(topBar);

//		tb.addMaterialCommandToSideMenu(" Test upload", FontImage.MATERIAL_EVENT_NOTE, e -> {
//			try {
//				System.out.println(SystemInfo.userInfo.getPresignedPutUrlPNG() + "<");
//
//				Image img = Image.createImage("/Club.png");
//				FileSystemStorage fss = FileSystemStorage.getInstance();
////	        boolean isJpeg = Image.isJPEG(fss.openInputStream(filePath));
//				boolean isPNG = true; // Image.isPNG(fss.openInputStream("/Club.png"));
//
//				String fileType = isPNG ? ".png" : ".jpeg";
//				SystemInfo.uploadToS3(isPNG ? SystemInfo.userInfo.getPresignedPutUrlPNG()
//						: SystemInfo.userInfo.getPresignedPutUrlJPEG(), "/Club.png", isPNG, img);
//			} catch (Exception eee) {
//				System.out.println(eee);
//			}
//		});
			tb.addMaterialCommandToSideMenu(" Today's Event", FontImage.MATERIAL_EVENT, e -> {
				form.removeAll();
				getTodaysEvents();
				form.revalidate();
			});
			tb.addMaterialCommandToSideMenu(" Available Events", FontImage.MATERIAL_EVENT_NOTE, e -> {
				loadAllEvents();
			});
			tb.addMaterialCommandToSideMenu(" My Interested Events", FontImage.MATERIAL_DATE_RANGE, e -> {
				loadMyInterestedEvents();
			});
			tb.addMaterialCommandToSideMenu(" My Booked Events", FontImage.MATERIAL_EVENT_AVAILABLE, e -> {
				loadMyBookedEvents();
			});
			tb.addMaterialCommandToSideMenu(" Get Last 30 Days", FontImage.MATERIAL_HISTORY, e -> {
				loadMyLast30DaysEvents();
			});

			if (SystemInfo.userInfo.getAccessLevel().equals("A")) {

//			tb.addMaterialCommandToSideMenu(" Filter Events", FontImage.MATERIAL_FILTER, e -> {
//				loadFilterEvents();
//			});

				tb.addMaterialCommandToSideMenu(" Unbooked Events", FontImage.MATERIAL_ADD_ALERT, e -> {
					loadUnbookedEvents();
				});

				tb.addMaterialCommandToSideMenu("Send Notification", FontImage.MATERIAL_NOTIFICATION_ADD, e -> {
					NotificationForm notificationForm = new NotificationForm(form, logoutScreen);
					notificationForm.show();
				});

//			tb.addMaterialCommandToSideMenu("Add New User", FontImage.MATERIAL_ADD, e -> {
//			NewUserForm newUserForm = new NewUserForm(form);
//			newUserForm.show();
//		});
//
//			tb.addMaterialCommandToSideMenu("Add New Event", FontImage.MATERIAL_ADD, e -> {
//				NewEventForm newEventForm = new NewEventForm(form);
//				newEventForm.show();
//			});
//
//			tb.addMaterialCommandToSideMenu(" Load Users", FontImage.MATERIAL_PEOPLE, e -> {
//				AllUsersForm allUsersForm = new AllUsersForm(form);
//				allUsersForm.show();
//			});
//			
//			tb.addMaterialCommandToSideMenu(" Load Events By Date", FontImage.MATERIAL_CALENDAR_VIEW_DAY, e ->{
//				EventByDateForm eventByDateForm = new EventByDateForm(form, logoutScreen);
//				eventByDateForm.show();
//			});
			}

			tb.addMaterialCommandToSideMenu(" My Profile", FontImage.MATERIAL_CONTACT_PAGE, e -> {
				loadUserInfo();
			});

			tb.addMaterialCommandToSideMenu(" About", FontImage.MATERIAL_INFO, e -> {
				AboutForm aboutForm = new AboutForm(form, logoutScreen);
				aboutForm.show();
			});

			tb.addMaterialCommandToSideMenu(" Log Out", FontImage.MATERIAL_HOME, e -> {
				// Clear the saved values if there are any.
				Preferences.set("userName", "");
				Preferences.set("pass", "");
				logoutScreen.show();
			});
		}
	}

	public void clearForm() {
		form.removeAll();
	}

	private void loadMyInterestedEvents() {
		form.removeAll();
		getUsersInterestedEvents();
		form.revalidate();
	}

	private void loadMyBookedEvents() {
		form.removeAll();
		getUsersBookedEvents();
		form.revalidate();
	}

	private void loadMyLast30DaysEvents() {
		form.removeAll();
		getUsersLastThirtyDaysEvents();
		form.revalidate();
	}

	private void loadUnbookedEvents() {
		form.removeAll();
		getUnbookedEvents();
		form.revalidate();
	}

	public void loadFilterEvents() {
		form.removeAll();
		getAllFilteredEvents();
		form.revalidate();
	}

	public void loadAllEvents() {
		form.removeAll();
		getAllEvents();
		form.revalidate();
	}

	private void loadUserInfo() {
		form.removeAll();
		getUserInfo();
		form.revalidate();
	}

	public void getUserInfo() {
		BackendCall loginReq = new BackendCall("loadUserInfo", false);
		HashMap<String, Object> args = new HashMap();
		args.put("userId", SystemInfo.userInfo.getId());
		Map<String, Object> result = loginReq.makeCall(args);
		ArrayList info = (ArrayList) result.get("root");

		UserForm userForm = new UserForm(form, logoutScreen);
		userForm.show();
	}

	public Map<String, Object> getUsersNotifications() {
		BackendCall usersNotificationsReq = new BackendCall("getNotificationsById", false);
		HashMap<String, Object> args = new HashMap();
		args.put("userId", SystemInfo.userInfo.getId());
		Map<String, Object> result = usersNotificationsReq.makeCall(args);

		ArrayList notifications = (ArrayList) result.get("root");
		return result;
	}

	public Map<String, Object> getUnbookedEvents() {
		form.setTitle("Unbooked Events");
		lastLoaded = "unbooked";
		BackendCall loginReq = new BackendCall("loadUnbookedEvents", false);
		HashMap<String, Object> args = new HashMap();
		Map<String, Object> result = loginReq.makeCall(args);
		ArrayList events = (ArrayList) result.get("root");
		displayEvents(events, true, false);
		return result;
	}

	public Map<String, Object> getAllFilteredEvents() {
		form.setTitle("Filtered Events");
		lastLoaded = "filteredEvents";
		BackendCall loginReq = new BackendCall("loadAllEvents", false);
		HashMap<String, Object> args = new HashMap();
		args.put("getOldEvents", "N");
		Map<String, Object> result = loginReq.makeCall(args);
		ArrayList events = (ArrayList) result.get("root");
		displayEvents(events, true, true, false, true);
		return result;
	}

	public Map<String, Object> getAllEvents() {
		form.setTitle("Available Events");
		lastLoaded = "allEvents";
		BackendCall loginReq = new BackendCall("loadAllEvents", false);
		HashMap<String, Object> args = new HashMap();
		args.put("getOldEvents", "N");
		Map<String, Object> result = loginReq.makeCall(args);
		ArrayList events = (ArrayList) result.get("root");
		displayEvents(events, true, true);
		return result;
	}

	public Map<String, Object> getSpecificEvent(String id) {
		form.setTitle("Event");
		lastLoaded = "notificationEvent";
		BackendCall loginReq = new BackendCall("loadSpecificEvent", false);
		HashMap<String, Object> args = new HashMap();
		args.put("eventId", id);
		Map<String, Object> result = loginReq.makeCall(args);
		ArrayList events = (ArrayList) result.get("root");
		displayEvents(events, false, false, true);
		return result;
	}

	// TODO The getUsers__Events methods can probs be combined
	public Map<String, Object> getUsersInterestedEvents() {
		form.setTitle("My Interested Events");
		lastLoaded = "usersInterestedEvents";
		BackendCall loginReq = new BackendCall("loadUsersInterestedEvents", false);
		HashMap<String, Object> args = new HashMap();
		args.put("userId", (int) SystemInfo.userInfo.getId() + "");
		Map<String, Object> result = loginReq.makeCall(args);
		int suitIndex = 0;
		ArrayList events = (ArrayList) result.get("root");
		displayEvents(events, false, false);
		form.revalidate();
		return result;
	}

	public Map<String, Object> getUsersBookedEvents() {
		form.setTitle("My Booked Events");
		lastLoaded = "usersBookedEvents";
		BackendCall loginReq = new BackendCall("loadUsersBookedEvents", false);
		HashMap<String, Object> args = new HashMap();
		args.put("userId", (int) SystemInfo.userInfo.getId() + "");
		Map<String, Object> result = loginReq.makeCall(args);
		int suitIndex = 0;
		ArrayList events = (ArrayList) result.get("root");
		displayEvents(events, false, false);
		form.revalidate();
		return result;
	}

	public Map<String, Object> getUsersLastThirtyDaysEvents() {
		form.setTitle("My Last 30 Days Events");
		lastLoaded = "usersLast30Events";
		BackendCall loginReq = new BackendCall("loadUsersEventsLastThirtyDays", false);
		HashMap<String, Object> args = new HashMap();
		args.put("userId", (int) SystemInfo.userInfo.getId() + "");
		Map<String, Object> result = loginReq.makeCall(args);
		int suitIndex = 0;
		ArrayList events = (ArrayList) result.get("root");
		displayEvents(events, false, false);
		form.revalidate();
		return result;
	}

	public Map<String, Object> getTodaysEvents() {
		lastLoaded = "todaysEvents";
		BackendCall loginReq = new BackendCall("loadTodaysEvents", false);
		HashMap<String, Object> args = new HashMap();
		args.put("userId", (int) SystemInfo.userInfo.getId() + "");
		String today = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		args.put("todaysDate", today);
		Map<String, Object> result = loginReq.makeCall(args);

		int suitIndex = 0;
		ArrayList events = (ArrayList) result.get("root");
		Label todaysEventsLabel = null;
		if (events.size() > 0) {
			form.setTitle("Today's Event!");
		} else {
			form.setTitle("No Event Today!");
		}
		displayEvents(events, false, false);

		form.revalidate();
		return result;
	}

	private void displayEvents(ArrayList events, boolean showInterestedStatesOnly, boolean isAvailableEventsPage) {
		displayEvents(events, showInterestedStatesOnly, isAvailableEventsPage, false);
	}

	private void displayEvents(ArrayList events, boolean showInterestedStatesOnly, boolean isAvailableEventsPage,
			boolean openEvent) {
		displayEvents(events, showInterestedStatesOnly, isAvailableEventsPage, openEvent, false);
	}

	private void displayEvents(ArrayList events, boolean showInterestedStatesOnly, boolean isAvailableEventsPage,
			boolean openEvent, boolean withFilterFields) {
		if (fab != null) {
			fab.remove();
		}
		int suitIndex = 0;
		Container eventsContainer = new Container(BoxLayout.y());

		eventsContainer.addPullToRefresh(() -> {
			displayEvents(events, showInterestedStatesOnly, isAvailableEventsPage);
		});

		eventsContainer.setScrollableY(true);
		eventsContainer.setUIID("backgroundWithImageNEW");
		form.setUIID("backgroundWithImageNEW");

		// TODO This is only a test for now. If they ever reload or click an event, all
		// new events will lose the NEW label. Plus we want it to look nicer - I think
		// this is working now but
		// tbh who knows. I don't remember writing this lol
		ArrayList<Integer> oldEvents = new ArrayList();
		String[] seenEventsStrings = SystemInfo.split(Preferences.get("seenEvents", ""), ",");
		for (int i = 0; i < seenEventsStrings.length; i++) {
			oldEvents.add(Integer.parseInt(seenEventsStrings[i]));
		}

		ArrayList<String> dontShowStatuses = getUserBookedStatuses();
		for (int i = 0; i < events.size(); i++) {
			EventInfo currEvent = new EventInfo((LinkedHashMap) events.get(i));

			int ossNeeded = Integer.parseInt(currEvent.getNumOssNeeded());
			int aosmNeeded = Integer.parseInt(currEvent.getNumAosmNeeded());
			int backupNeeded = Integer.parseInt(currEvent.getNumBackupsNeeded());
			int osmtNeeded = Integer.parseInt(currEvent.getNumOsmtNeeded());
			int spotterNeeded = Integer.parseInt(currEvent.getNumSpotterNeeded());
			int scriberNeeded = Integer.parseInt(currEvent.getNumScriberNeeded());
			int osmNeeded = Integer.parseInt(currEvent.getNumOsmNeeded());

			boolean dontShowEvent = false;
//			if (isAvailableEventsPage) {
			ArrayList users = currEvent.getUsers();
			for (int u = 0; u < users.size(); u++) {
				UserInfo currUser = new UserInfo((LinkedHashMap) users.get(u));
				if (currUser.getStatus().equals("OSS")) {
					ossNeeded--;
				} else if (currUser.getStatus().equals("AOSM")) {
					aosmNeeded--;
				} else if (currUser.getStatus().equals("Backup")) {
					backupNeeded--;
				} else if (currUser.getStatus().equals("OSM")) {
					osmNeeded--;
				}else if (currUser.getStatus().equals("OSMT")) {
					osmtNeeded--;
				}else if (currUser.getStatus().equals("Spotter")) {
					spotterNeeded--;
				}else if (currUser.getStatus().equals("Scriber")) {
					scriberNeeded--;
				}
				if (isAvailableEventsPage) {
					if (SystemInfo.userInfo.getId() == currUser.getId()) {
						if (dontShowStatuses.contains(currUser.getStatus())) {
							dontShowEvent = true;
						}
					}
				}
			}

			boolean fullyBooked = osmNeeded <= 0 && aosmNeeded <= 0 && ossNeeded <= 0 && backupNeeded <= 0 && osmtNeeded <= 0 && spotterNeeded <= 0 && scriberNeeded <= 0;

			boolean isNew = false;
			if (isAvailableEventsPage) {
				if (!oldEvents.contains(currEvent.getId())) {
					isNew = true;
				} else {
					updatedSendEventsString += currEvent.getId() + ",";
				}
			}

			if (!dontShowEvent) {
				SpanMultiButton eventMultiButton = new SpanMultiButton();
				eventMultiButton.setTextLine1(currEvent.getFormattedEventDate());
				eventMultiButton.setTextLine2(currEvent.getAddress());
				eventMultiButton
						.setTextLine3(currEvent.getCity() + ", " + currEvent.getState() + ", " + currEvent.getZip());
				eventMultiButton.setTextLine4(
						(SystemInfo.userInfo.getAccessLevel().equals("A") ? currEvent.getOrganization().trim() + " - "
								: "") + (fullyBooked ? "*Fully Staffed*" : "*Staff Needed*"));
				eventMultiButton.setUIIDLine1(isNew ? "NewEventMultiButtonDate" : "EventMultiButtonDate");
				eventMultiButton.setUIIDLine2(isNew ? "EventMultiButtonLabelLight" : "EventMultiButton");
				eventMultiButton.setUIIDLine3(isNew ? "EventMultiButtonLabelLight" : "EventMultiButton");
				eventMultiButton.setUIIDLine4(isNew ? "EventMultiButtonLabelLight2" : "EventMultiButtonLabel2");
//				eventMultiButton.setUIID("EventMultiButtonNewEvent");
				eventMultiButton.setUIID(isNew ? "EventMultiButtonLabelLight" : "EventMultiButton");

				eventMultiButton.addActionListener(action -> {
					updatedSendEventsString += currEvent.getId() + ",";
					if (isAvailableEventsPage) {
						Preferences.set("seenEvents", updatedSendEventsString);
					}
					eventMultiButton.setUIIDLine1("EventMultiButtonDate");
					eventMultiButton.setUIIDLine2("EventMultiButton");
					eventMultiButton.setUIIDLine3("EventMultiButton");
					eventMultiButton.setUIIDLine4("EventMultiButtonLabel2");
					eventMultiButton.setUIID("EventMultiButton");
					SingleEventForm singleEventForm = new SingleEventForm(form, currEvent, logoutScreen);
					singleEventForm.show();
				});
				if (!showInterestedStatesOnly) {
					eventsContainer.add(eventMultiButton);
				} else {
					for (LinkedHashMap stateInfo : SystemInfo.userInfo.getStates()) {
						if (stateInfo.get("abbreviation").equals(currEvent.getState())) {
							eventsContainer.add(eventMultiButton);
						}
					}
				}
				if (openEvent) {
					updatedSendEventsString += currEvent.getId() + ",";
					if (isAvailableEventsPage) {
						Preferences.set("seenEvents", updatedSendEventsString);
					}
					SingleEventForm singleEventForm = new SingleEventForm(form, currEvent, logoutScreen);
					singleEventForm.show();
					return;
				}
			}
		}
		if (isAvailableEventsPage) {
			Preferences.set("seenEvents", updatedSendEventsString);
		}
		if (withFilterFields) { // TODO Finish this
			Container filterContainer = new Container(BoxLayout.y());

			filterCityField.addActionListener(action -> {
				ArrayList filteredEvents = new ArrayList();
				System.out.println("Displaying events");
				for (int j = 0; j < events.size(); j++) {
					System.out.println(filterCityField.getText());
					System.out.println(stateFilterField.getText());
					System.out.println(dateFilterPicker.getText());

					EventInfo currEvent = new EventInfo((LinkedHashMap) events.get(j));
					System.out.println(currEvent.getEventDate());
					System.out.println(currEvent.getCity());
					System.out.println(currEvent.getState());

					// TODO Format the field values to match the event values and only add events
					// that match.
				}
				filterContainer.removeAll();
				displayEvents(filteredEvents, showInterestedStatesOnly, isAvailableEventsPage, openEvent,
						withFilterFields);
			});
			filterContainer.add(filterCityField);

			stateFilterField.setStrings(getStates());
			filterContainer.add(stateFilterField);

			dateFilterPicker.setType(Display.PICKER_TYPE_DATE);
			filterContainer.add(dateFilterPicker);

			form.add(BorderLayout.NORTH, filterContainer);
		}
		form.add(BorderLayout.CENTER, eventsContainer);

		Label msgLabel = new Label("fundraising with heart", "redLabelLightBlueBackgroundPadded");
		form.add(BorderLayout.SOUTH, msgLabel);

		if (!openEvent) {
			form.revalidate();
		}

		refreshNotifications();
	}

	private void refreshNotifications() {
		Map<String, Object> notifications = getUsersNotifications();
		ActionListener notificationActionListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				ArrayList notificationsList = (ArrayList) notifications.get("root");
				if (notificationsList.size() > 0) {
					UserNotificationsForm userNotificationsForm = new UserNotificationsForm(form,
							(ArrayList) notificationsList, logoutScreen);
					userNotificationsForm.show();
				}

//				Dialog dialog = new Dialog("Notifications") {
//					@Override
//					public void dispose() {
//						refreshNotifications();
//						super.dispose();
//					}
//				};
//				dialog.setUIID("HeaderBackground");
//				dialog.setDialogUIID("HeaderBackground");
//				dialog.getTitleComponent().setUIID("Label");
//				dialog.setDisposeWhenPointerOutOfBounds(true);
//				Container notificationContainer = new Container();
//				notificationContainer.setScrollableY(true);
//				ArrayList notificationsList = (ArrayList) notifications.get("root");
//				if (notificationsList.size() > 0) {
//					for (int i = 0; i < notificationsList.size(); i++) {
//						HashMap<String, Object> currNotification = (HashMap<String, Object>) notificationsList.get(i);
//
//						Container singleNotificationContainer = new Container(new BorderLayout());
//						Label notificationLabel = new Label(
//								currNotification.get("message") + "                                    ",
//								"Label"); // was notificationLabel
//						notificationLabel.addPointerReleasedListener(action -> {
//							if ((currNotification.get("notificationDetails") + "").trim().length() > 0) {
//								dialog.dispose();
//								getSpecificEvent("" + currNotification.get("notificationDetails"));
//								lastLoaded = "allEvents";
//							}
//						});
////						Button notificationButton = new Button("Delete");
////						notificationButton.setUIID("FatButton");
//						Label deleteNotificationLabelButton = new Label("");
//						deleteNotificationLabelButton.addPointerReleasedListener(event -> {
//							notificationContainer.removeComponent(singleNotificationContainer);
//							notificationContainer.revalidate();
//							deleteNotification((Double) currNotification.get("id"));
//							if (notificationContainer.getComponentCount() == 2) { // 2 because the labels at the bottom
//								dialog.dispose();
//							}
//						});
//						
//						Image deleteNotificationLabelButtonIcon = FontImage.createMaterial(FontImage.MATERIAL_REMOVE_CIRCLE, "Label",
//								7);
//						deleteNotificationLabelButton.setIcon(deleteNotificationLabelButtonIcon);
//						singleNotificationContainer.add(BorderLayout.WEST, notificationLabel);
//						singleNotificationContainer.add(BorderLayout.EAST, deleteNotificationLabelButton);
//
//						notificationContainer.add(singleNotificationContainer);
//					}
//					notificationContainer.add(new Label(" "));
//					notificationContainer.add(new Label(" "));
//					dialog.add(new Label(" "));
//					dialog.add(new Label(" "));
//					dialog.add(notificationContainer);
//					dialog.show();
//				}
			}
		};

		Image notificationIcon = FontImage.createMaterial(FontImage.MATERIAL_NOTIFICATIONS, "TitleNotificationCommand",
				5);

		ArrayList numNotifications = (ArrayList) notifications.get("root");
		if (numNotifications != null && numNotifications.size() > 0) {
			notificationIcon = FontImage.createMaterial(FontImage.MATERIAL_NOTIFICATION_ADD,
					"TitleNewNotificationCommand", 4);
		}

		if (notificationCommand != null) {
			tb.removeCommand(notificationCommand);
		}
		notificationCommand = Command.create("  ", notificationIcon, notificationActionListener);
		tb.addCommandToRightBar(notificationCommand);
		this.form.revalidate();
		this.form.getToolbar().revalidate();
	}

	private ArrayList<String> getUserBookedStatuses() {
		ArrayList<String> bookedStatuses = new ArrayList<String>();
		bookedStatuses.add("OSM");
		bookedStatuses.add("AOSM");
		bookedStatuses.add("OSS");
		bookedStatuses.add("Backup");
		bookedStatuses.add("OSMT");
		bookedStatuses.add("Spotter");
		bookedStatuses.add("Scriber");
		bookedStatuses.add("Available");
		return bookedStatuses;
	}

	public String getLastLoaded() {
		return lastLoaded;
	}

	public void setLastLoaded(String formName) {
		lastLoaded = formName;
	}

	private String[] getStates() {
		String[] abbrevs = { " ", "Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut",
				"Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas",
				"Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi",
				"Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York",
				"North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island",
				"South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington",
				"West Virginia", "Wisconsin", "Wyoming" };
		return abbrevs;
	}
}