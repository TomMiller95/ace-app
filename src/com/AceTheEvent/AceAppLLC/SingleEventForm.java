package com.AceTheEvent.AceAppLLC;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.codename1.capture.Capture;
import com.codename1.components.FloatingActionButton;
import com.codename1.components.SpanMultiButton;
import com.codename1.components.ToastBar;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.FileSystemStorage;
import com.codename1.io.JSONParser;
import com.codename1.io.MultipartRequest;
import com.codename1.io.NetworkManager;
import com.codename1.io.Util;
import com.codename1.location.Location;
import com.codename1.location.LocationManager;
import com.codename1.ui.Button;
import com.codename1.ui.CheckBox;
import com.codename1.ui.Component;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Tabs;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.URLImage;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.table.TableLayout;
import com.codename1.util.Base64;
import com.codename1.util.StringUtil;

public class SingleEventForm extends Form {

	private EventInfo eventInfo = null;
	private boolean userFound = false;
	private String status = "";
	private String url = "";
	private Form logoutScreen = null;
	private boolean managerCheckedOutAlready = false;
	String userEnteredCheckInTime = "";
	String userEnteredCheckOutTime = "";

	private HashMap<Integer, Picker> adminContractorStatusFields = new HashMap();
	private SideMenuButton menuButton = null;

	public SingleEventForm(Form backForm, EventInfo eventInfo, Form logoutScreen) {
		super("Event Information", new BorderLayout());
		this.eventInfo = eventInfo;
		this.logoutScreen = logoutScreen;
		this.setUIID("EventContainer");

		FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_ARROW_BACK);
		fab.addActionListener(e -> {
			backForm.show();
		});
		fab.setUIID("Fab");
		fab.bindFabToContainer(this.getContentPane(), Component.LEFT, Component.BOTTOM);

		menuButton = new SideMenuButton(this, backForm, logoutScreen, fab, true);

		if (SystemInfo.userInfo.getAccessLevel().equals("A")) {
			addAdminFields();
		} else {
			addUserFields();
		}
	}

	private void addAdminFields() {
		String header = eventInfo.getOrganization();

		TextArea headerLabel = new TextArea(header);
		if (header.trim().length() > 30) {
			headerLabel.setUIID("OrganizationHeader");
		} else {
			headerLabel.setUIID("OrganizationHeaderLarge");
		}
		headerLabel.setEnabled(false);

		Label ossTimeLabel = new Label("OSS Time: " + eventInfo.getOssStartTime() + " - " + eventInfo.getOssEndTime(),
				"WhiteLabelCentered");
		Label osmTimeLabel = new Label("OSM Time: " + eventInfo.getOsmStartTime() + " - " + eventInfo.getOsmEndTime(),
				"WhiteLabelCentered");
		Label spotterTimeLabel = new Label(
				"Spotter Time: " + eventInfo.getSpotterStartTime() + " - " + eventInfo.getSpotterEndTime(),
				"WhiteLabelCentered");
		Label scriberTimeLabel = new Label(
				"Scriber Time: " + eventInfo.getScriberStartTime() + " - " + eventInfo.getScriberEndTime(),
				"WhiteLabelCentered");
//		Label backupTimeLabel = new Label(eventInfo.getBackupStartTime() + " - " + eventInfo.getBackupEndTime(),
//				"WhiteEventLabel");
		Label eventDateLabel = new Label(eventInfo.getFormattedEventDate(), "WhiteLabelCentered");

		SpanMultiButton addressField = new SpanMultiButton();
		addressField.setTextLine1(eventInfo.getAddress() + ",");
		addressField.setTextLine2(eventInfo.getCity() + ", " + eventInfo.getState() + ", " + eventInfo.getZip());
		addressField.setUIID("MapsLinkButtonCentered");
		addressField.setIconUIID("MapsLinkButtonCentered");
		addressField.setUIIDLine1("MapsLinkButtonCentered");
		addressField.setUIIDLine2("MapsLinkButtonCentered");
		TextArea venueLabel = new TextArea(eventInfo.getVenue());

//		Label notesHeaderLabel = new Label("__Notes__", "WhiteDarkLabelCentered");
		TextArea notesLabel = new TextArea(eventInfo.getNotes());
		TextArea eventDescLabel = new TextArea(eventInfo.getDescription());
		TextArea publicDescLabel = new TextArea(eventInfo.getPublicDesc());
		TextArea managerNotesLabel = new TextArea(eventInfo.getManagerNote());
		notesLabel.setUIID("WhiteEventLabel");
		eventDescLabel.setUIID("WhiteLabelCentered");
		publicDescLabel.setUIID("WhiteEventLabel");
		managerNotesLabel.setUIID("WhiteEventLabel");
		venueLabel.setUIID("WhiteLabelCentered");
		venueLabel.setEditable(false);
		notesLabel.setEditable(false);
		notesLabel.setFocusable(false);
		eventDescLabel.setEditable(false);
		eventDescLabel.setFocusable(false);
		publicDescLabel.setEditable(false);
		publicDescLabel.setFocusable(false);
		managerNotesLabel.setEditable(false);
		managerNotesLabel.setFocusable(false);

		FontImage eventDateIcon = FontImage.createMaterial(FontImage.MATERIAL_CALENDAR_TODAY, "WhiteEventLabel", 4);
		FontImage venueIcon = FontImage.createMaterial(FontImage.MATERIAL_HOUSE, "WhiteEventLabel", 4);
		FontImage notesIcon = FontImage.createMaterial(FontImage.MATERIAL_NOTES, "WhiteEventLabel", 4);
		FontImage timeIcon = FontImage.createMaterial(FontImage.MATERIAL_TIMER, "WhiteEventLabel", 4);
		FontImage addressIcon = FontImage.createMaterial(FontImage.MATERIAL_LOCATION_PIN, "WhiteEventLabel", 4);
		eventDateLabel.setIcon(eventDateIcon);
		osmTimeLabel.setIcon(timeIcon);
		spotterTimeLabel.setIcon(timeIcon);
		scriberTimeLabel.setIcon(timeIcon);
		ossTimeLabel.setIcon(timeIcon);
//		backupTimeLabel.setIcon(timeIcon);
//		addressField.setIcon(addressIcon);

		Tabs tabs = new Tabs();
		tabs.setUIID("WhiteEventLabel");
		Container eventTab = new Container(BoxLayout.y());

		ArrayList users = eventInfo.getUsers();
		setUsersStatus(users);

		addBlankLines(eventTab, 1);

		if (!(status.trim().equals("Available") || status.trim().equals("Denied") || status.trim().equals(""))) {
			Label statusLabel = new Label("You are booked as " + status, "PurpleLabelGoldFontCentered");
			eventTab.add(statusLabel);
			addBlankLines(eventTab, 1);
		}

		HashMap<String, Object> checkInInfo = getCheckInButtonStatus(status);
		boolean showCheckInButton = (boolean) checkInInfo.get("canCheckIn");
		boolean showCheckOutButton = (boolean) checkInInfo.get("canCheckOut");
		String checkedInTime = (String) checkInInfo.get("checkedInTime");
		String checkedOutTime = (String) checkInInfo.get("checkedOutTime");
		if (checkedOutTime.trim().length() > 0) {
			managerCheckedOutAlready = true;
			Button checkOutButton = new Button("Checked Out Already");
			checkOutButton.setTickerEnabled(false);
			checkOutButton.setUIID("FatButton");
			eventTab.add(checkOutButton);
		} else if (showCheckOutButton) {
			Button checkOutButton = new Button("Check Out");
			checkOutButton.setTickerEnabled(false);
			checkOutButton.setUIID("FatButton");
			checkOutButton.addActionListener(event -> {
				if (sendCheckInOutRequest(false)) {
					managerCheckedOutAlready = true;
					checkOutButton.remove();
					this.repaint();
					this.refreshTheme();
				}
			});
			eventTab.add(checkOutButton);
		} else if (checkedInTime.trim().length() > 0) {
			Button checkOutButton = new Button("Checked In Already");
			checkOutButton.setTickerEnabled(false);
			checkOutButton.setUIID("FatButton");
			eventTab.add(checkOutButton);
		} else if (showCheckInButton) {
			Button checkinButton = new Button("Check In");
			checkinButton.setTickerEnabled(false);
			checkinButton.setUIID("FatButton");
			checkinButton.addActionListener(event -> {
				if (sendCheckInOutRequest(true)) {
					checkinButton.remove();
					this.repaint();
					this.refreshTheme();
				}
			});
			eventTab.add(checkinButton);
		}

		if (eventDescLabel.getText().trim().length() > 0) {
			eventTab.add(eventDescLabel);
			addBlankLines(eventTab, 1);
		}

		eventTab.add(venueLabel);
//		eventTab.add(venueLabel);
		addBlankLines(eventTab, 1);

		eventTab.add(eventDateLabel);
		addBlankLines(eventTab, 1);

		if (SystemInfo.userInfo.getAccessLevel().equals("M") || SystemInfo.userInfo.getAccessLevel().equals("T")
				|| SystemInfo.userInfo.getAccessLevel().equals("A")) {
			if (eventInfo.getOsmStartTime().trim().length() > 0) {
				eventTab.add(osmTimeLabel);
			}
		}
		if (eventInfo.getOssStartTime().trim().length() > 0) {
			eventTab.add(ossTimeLabel);
		}
		if (eventInfo.getSpotterStartTime().trim().length() > 0) {
			eventTab.add(spotterTimeLabel);
		}
		if (eventInfo.getScriberStartTime().trim().length() > 0) {
			eventTab.add(scriberTimeLabel);
		}
		addBlankLines(eventTab, 1);

		Button availableButton = new Button(userFound ? "Unregister" : "Interested - Deal Me In!");
		availableButton.setTickerEnabled(false);
		availableButton.setUIID(userFound ? "UnregisterButton" : "FatButton");
		availableButton.addActionListener(event -> {

			Dialog dialog = new Dialog(" Which positions are you interested in? ");
			dialog.setUIID("DialogBackground");
			dialog.setDialogUIID("DialogBackground");
			dialog.getTitleComponent().setUIID("StatusDialogLabelUnselected");
			Label ossLabel = new Label("On Site Support", "StatusDialogLabelUnselected");
			Label osmLabel = new Label("On Site Manager", "StatusDialogLabelUnselected");
			Label aosmLabel = new Label("Assistant On Site Manager", "StatusDialogLabelUnselected");
			Label backupLabel = new Label("Backup", "StatusDialogLabelUnselected");
			Label osmtLabel = new Label("On Site Manager Trainee", "StatusDialogLabelUnselected");
			Label spotterLabel = new Label("Spotter", "StatusDialogLabelUnselected");
			Label scriberLabel = new Label("Scriber", "StatusDialogLabelUnselected");

			FontImage ossIcon = FontImage.createMaterial(FontImage.MATERIAL_ADD, "StatusDialogLabelSelected", 6);
			ossLabel.setIcon(ossIcon);
			FontImage osmIcon = FontImage.createMaterial(FontImage.MATERIAL_ADD, "StatusDialogLabelSelected", 6);
			osmLabel.setIcon(osmIcon);
			FontImage aosmIcon = FontImage.createMaterial(FontImage.MATERIAL_ADD, "StatusDialogLabelSelected", 6);
			aosmLabel.setIcon(aosmIcon);
			FontImage backupIcon = FontImage.createMaterial(FontImage.MATERIAL_ADD, "StatusDialogLabelSelected", 6);
			backupLabel.setIcon(backupIcon);
			FontImage osmtIcon = FontImage.createMaterial(FontImage.MATERIAL_ADD, "StatusDialogLabelSelected", 6);
			osmtLabel.setIcon(osmtIcon);
			FontImage spotterIcon = FontImage.createMaterial(FontImage.MATERIAL_ADD, "StatusDialogLabelSelected", 6);
			spotterLabel.setIcon(spotterIcon);
			FontImage scriberIcon = FontImage.createMaterial(FontImage.MATERIAL_ADD, "StatusDialogLabelSelected", 6);
			spotterLabel.setIcon(scriberIcon);

			Label submitButton = new Label("Cancel", "GoldLabelLightBackground");
			ossLabel.addPointerReleasedListener(action -> {
				boolean selected = ossLabel.getUIID().equals("StatusDialogLabelUnselected")
						|| osmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| aosmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| backupLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmtLabel.getUIID().equals("StatusDialogLabelSelected")
						|| spotterLabel.getUIID().equals("StatusDialogLabelSelected")
						|| scriberLabel.getUIID().equals("StatusDialogLabelSelected");
				checkOffLabel(ossLabel, dialog, submitButton, selected);
			});
			osmLabel.addPointerReleasedListener(action -> {
				boolean selected = ossLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmLabel.getUIID().equals("StatusDialogLabelUnselected")
						|| aosmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| backupLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmtLabel.getUIID().equals("StatusDialogLabelSelected")
						|| spotterLabel.getUIID().equals("StatusDialogLabelSelected")
						|| scriberLabel.getUIID().equals("StatusDialogLabelSelected");
				checkOffLabel(osmLabel, dialog, submitButton, selected);
			});
			aosmLabel.addPointerReleasedListener(action -> {
				boolean selected = ossLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| aosmLabel.getUIID().equals("StatusDialogLabelUnselected")
						|| backupLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmtLabel.getUIID().equals("StatusDialogLabelSelected")
						|| spotterLabel.getUIID().equals("StatusDialogLabelSelected")
						|| scriberLabel.getUIID().equals("StatusDialogLabelSelected");
				checkOffLabel(aosmLabel, dialog, submitButton, selected);
			});
			backupLabel.addPointerReleasedListener(action -> {
				boolean selected = ossLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| aosmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| backupLabel.getUIID().equals("StatusDialogLabelUnselected")
						|| osmtLabel.getUIID().equals("StatusDialogLabelSelected")
						|| spotterLabel.getUIID().equals("StatusDialogLabelSelected")
						|| scriberLabel.getUIID().equals("StatusDialogLabelSelected");
				checkOffLabel(backupLabel, dialog, submitButton, selected);
			});
			osmtLabel.addPointerReleasedListener(action -> {
				boolean selected = ossLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| aosmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| backupLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmtLabel.getUIID().equals("StatusDialogLabelUnselected")
						|| spotterLabel.getUIID().equals("StatusDialogLabelSelected")
						|| scriberLabel.getUIID().equals("StatusDialogLabelSelected");
				checkOffLabel(osmtLabel, dialog, submitButton, selected);
			});
			spotterLabel.addPointerReleasedListener(action -> {
				boolean selected = ossLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| aosmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| backupLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmtLabel.getUIID().equals("StatusDialogLabelUnselected")
						|| spotterLabel.getUIID().equals("StatusDialogLabelUnselected")
						|| scriberLabel.getUIID().equals("StatusDialogLabelSelected");
				checkOffLabel(spotterLabel, dialog, submitButton, selected);
			});
			scriberLabel.addPointerReleasedListener(action -> {
				boolean selected = ossLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| aosmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| backupLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmtLabel.getUIID().equals("StatusDialogLabelUnselected")
						|| spotterLabel.getUIID().equals("StatusDialogLabelSelected")
						|| scriberLabel.getUIID().equals("StatusDialogLabelUnselected");
				checkOffLabel(scriberLabel, dialog, submitButton, selected);
			});

//			Label cancelButton = new Label("Cancel", "GoldLabelLightBackground");

//			Label noFieldsPickedLabel = new Label("      PLEASE SELECT A STATUS      ");
//			noFieldsPickedLabel.setVisible(false);
//			Label spacingLabel = new Label(" ");
//			spacingLabel.setVisible(false);

			submitButton.addPointerReleasedListener(action -> {
				dialog.dispose();
				if (ossLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| aosmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| backupLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmtLabel.getUIID().equals("StatusDialogLabelSelected")
						|| spotterLabel.getUIID().equals("StatusDialogLabelSelected")
						|| scriberLabel.getUIID().equals("StatusDialogLabelSelected")) {
					dialog.dispose();
					setAvailable(!userFound, ossLabel.getUIID().equals("StatusDialogLabelSelected"),
							osmLabel.getUIID().equals("StatusDialogLabelSelected"),
							aosmLabel.getUIID().equals("StatusDialogLabelSelected"),
							backupLabel.getUIID().equals("StatusDialogLabelSelected"),
							osmtLabel.getUIID().equals("StatusDialogLabelSelected"),
							spotterLabel.getUIID().equals("StatusDialogLabelSelected"),
							scriberLabel.getUIID().equals("StatusDialogLabelSelected"));
					if (userFound) {
						availableButton.setText("Interested - Deal Me In!");
					} else {
						availableButton.setText("No Longer Interested");
					}
					availableButton.setUIID(userFound ? "FatButton" : "UnregisterButton");
					userFound = !userFound;
					this.revalidate();
				}
//				else {
////					noFieldsPickedLabel.setVisible(true);
////					spacingLabel.setVisible(true);
//					dialog.revalidate();
//				}
			});
//			cancelButton.addPointerReleasedListener(action -> {
//				dialog.dispose();
//			});

			if (availableButton.getText().equals("Interested - Deal Me In!")) {
				Container positionsContainer = new Container(BoxLayout.y());
				positionsContainer.setUIID("DialogBackground");

				positionsContainer.add(ossLabel);

				// If Admin or Manager, add these options
				if (SystemInfo.userInfo.getAccessLevel().equals("M") || SystemInfo.userInfo.getAccessLevel().equals("T")
						|| SystemInfo.userInfo.getAccessLevel().equals("A")) {
					positionsContainer.add(osmLabel).add(aosmLabel);
					positionsContainer.add(osmtLabel);
				}
				if (SystemInfo.userInfo.getSpotterStatus().equals("true")) {
					positionsContainer.add(spotterLabel);
				}
				if (SystemInfo.userInfo.getScriberStatus().equals("true")) {
					positionsContainer.add(scriberLabel);
				}
				positionsContainer.add(backupLabel);// .add(noFieldsPickedLabel);//.add(spacingLabel).add(noFieldsPickedLabel);

				Container buttonsContainer = new Container(BoxLayout.y());
				buttonsContainer.setUIID("DialogBackground");
				buttonsContainer.add(submitButton);// .add(cancelButton);
				positionsContainer.add(buttonsContainer);
				positionsContainer.add(new Label(
						"                                                                                                                                                          ")); // I
																																														// hate
																																														// this
				dialog.add(positionsContainer);
				dialog.setDisposeWhenPointerOutOfBounds(false);
				dialog.show();
			} else {
				setAvailable(false, false, false, false, false, false, false, false);
				if (userFound) {
					availableButton.setText("Interested - Deal Me In!");
					availableButton.setUIID("FatButton");
				} else {
					availableButton.setUIID("UnregisterButton");
				}
				userFound = !userFound;
				this.revalidate();
			}
		});

		url = "https://www.google.com/maps/search/" + eventInfo.getAddress() + ", " + eventInfo.getCity() + ", "
				+ eventInfo.getState() + ", " + eventInfo.getZip().trim();
		addressField.addActionListener(action -> {
			url = StringUtil.replaceAll(url, " ", "%20");
			Display.getInstance().execute(url);
		});
		eventTab.add(addressField);

		int screenWidth = Display.getInstance().getDisplayWidth();
		int screenHeight = Display.getInstance().getDisplayHeight();

		int targetWidth = screenWidth / 3;
		int targetHeight = screenHeight / 7;

		try {
//			Image defaultImage = Image.createImage("/checkedHeart.jpg");
			Image defaultImage = Image.createImage("/checkedHeartCentered.jpg");
			Image image = defaultImage.scaledWidth(targetWidth);
			Label star = new Label(image);
			eventTab.add(star);
			addBlankLines(eventTab, 1);
		} catch (Exception e) {
			System.out.println(e);
		}

		if (!status.trim().equals("Available") && !status.trim().equals("Denied") && !status.trim().equals("")) {
			if (managerNotesLabel.getText().trim().length() > 0) {
				eventTab.add(new Label("__Manager Notes__", "WhiteDarkLabelCentered"));
				addBlankLines(eventTab, 1);
				eventTab.add(managerNotesLabel);
				addBlankLines(eventTab, 1);
			}
			if (notesLabel.getText().trim().length() > 0) {
				eventTab.add(new Label("__General Notes__", "WhiteDarkLabelCentered"));
				addBlankLines(eventTab, 1);
				eventTab.add(notesLabel);
				addBlankLines(eventTab, 1);
			}
			if (publicDescLabel.getText().trim().length() > 0) {
				eventTab.add(publicDescLabel);
				addBlankLines(eventTab, 1);
			}
		} else {
			// Figure out how many OSS, OSM, AOSM, etc are needed.
			int ossNeeded = Integer.parseInt(eventInfo.getNumOssNeeded());
			int aosmNeeded = Integer.parseInt(eventInfo.getNumAosmNeeded());
			int backupNeeded = Integer.parseInt(eventInfo.getNumBackupsNeeded());
			int osmtNeeded = Integer.parseInt(eventInfo.getNumOsmtNeeded());
			int osmNeeded = Integer.parseInt(eventInfo.getNumOsmNeeded());
			int spotterNeeded = Integer.parseInt(eventInfo.getNumSpotterNeeded());
			int scriberNeeded = Integer.parseInt(eventInfo.getNumScriberNeeded());
			for (int i = 0; i < users.size(); i++) {
				UserInfo currUser = new UserInfo((LinkedHashMap) users.get(i));
				if (currUser.getStatus().equals("OSS")) {
					ossNeeded--;
				} else if (currUser.getStatus().equals("AOSM")) {
					aosmNeeded--;
				} else if (currUser.getStatus().equals("Backup")) {
					backupNeeded--;
				} else if (currUser.getStatus().equals("OSMT")) {
					osmtNeeded--;
				} else if (currUser.getStatus().equals("OSM")) {
					osmNeeded--;
				} else if (currUser.getStatus().equals("Spotter")) {
					spotterNeeded--;
				} else if (currUser.getStatus().equals("Scriber")) {
					scriberNeeded--;
				}
			}
			addBlankLines(eventTab, 1);
			if (osmNeeded > 0) {
				Label totalOsmNeeded = new Label("OSM Needed: " + osmNeeded, "WhiteLabelCentered");
				eventTab.add(totalOsmNeeded);
			}
			if (aosmNeeded > 0) {
				Label totalAosmNeeded = new Label("AOSM Needed: " + aosmNeeded, "WhiteLabelCentered");
				eventTab.add(totalAosmNeeded);
			}
			if (osmtNeeded > 0) {
				Label totalOsmtNeeded = new Label("OSMT Needed: " + osmtNeeded, "WhiteLabelCentered");
				eventTab.add(totalOsmtNeeded);
			}
			if (ossNeeded > 0) {
				Label totalOssNeeded = new Label("OSS Needed: " + ossNeeded, "WhiteLabelCentered");
				eventTab.add(totalOssNeeded);
				if (eventInfo.getOssAmount().trim().length() > 0) {
					Label ossPayLabel = new Label("OSS Event Pay: $" + eventInfo.getOssAmount(), "WhiteLabelCentered");
					eventTab.add(ossPayLabel);
				}
			}
			if (spotterNeeded > 0) {
				Label totalSpotterNeeded = new Label("Spotters Needed: " + spotterNeeded, "WhiteLabelCentered");
				eventTab.add(totalSpotterNeeded);
			}
			if (scriberNeeded > 0) {
				Label totalScriberNeeded = new Label("Scribers Needed: " + scriberNeeded, "WhiteLabelCentered");
				eventTab.add(totalScriberNeeded);
			}
			if (backupNeeded > 0) {
				Label totalBackupNeeded = new Label("Backups Needed: " + backupNeeded, "WhiteLabelCentered");
				eventTab.add(totalBackupNeeded);
			}

			if (osmNeeded <= 0 && aosmNeeded <= 0 && ossNeeded <= 0 && backupNeeded <= 0 && osmtNeeded <= 0
					&& spotterNeeded <= 0 && scriberNeeded <= 0) {
				TextArea allBookedLabel = new TextArea(
						"Fully Staffed!\nContinued sign-ups are encouraged to accommodate any potential staffing changes.");
				allBookedLabel.setUIID("WhiteLabelCentered");
				allBookedLabel.setEditable(false);
				eventTab.add(allBookedLabel);
			}

			addBlankLines(eventTab, 2);

			eventTab.add(new Label("General Notes", "WhiteEventLabel"));
			eventTab.add(notesLabel);
			eventTab.add(publicDescLabel);
		}

//		addBlankLines(eventTab, 5);

		Container adminEventTab = new Container(BoxLayout.y());
		for (int i = 0; i < users.size(); i++) {
			UserInfo currUser = new UserInfo((LinkedHashMap) users.get(i));

			Label userInfoField = new Label(currUser.getFirstName() + " " + currUser.getLastName(),
					"WhiteLabelCentered");

			Picker statusField = new Picker();
//			statusField.setUIID("ComboBox");
			statusField.setStrings(getStatuses(currUser));
			statusField.setSelectedString(currUser.getStatus());

			Label tmpSpace = new Label(" ", "WhiteEventLabel");

			adminEventTab.add(userInfoField);
			adminEventTab.add(statusField);
			adminEventTab.add(tmpSpace);

			adminContractorStatusFields.put(currUser.getId(), statusField);
		}

		Button updateStatusesButton = new Button("Save Contractor Statuses", "FatButton");
		updateStatusesButton.setTickerEnabled(false);
		updateStatusesButton.addActionListener(action -> {
			String conflictingTime = "";
			for (Map.Entry<Integer, Picker> entry : adminContractorStatusFields.entrySet()) {
				int key = entry.getKey();
				Picker value = entry.getValue();
				String result = setStatus(key, value.getSelectedString());
				if (result.trim().length() > 0) {
					value.setSelectedString("Available");
					conflictingTime += result + "\n";
				}
			}

			Dialog dialog = new Dialog("Contractor statuses saved!");
			dialog.setUIID("DialogBackground");
			dialog.setDialogUIID("DialogBackground");
			dialog.getTitleComponent().setUIID("StatusDialogLabelUnselected");

			Label errorLabel = new Label(conflictingTime);
			Button okButton = new Button("Ok");
			okButton.setTickerEnabled(false);
			Container buttonsContainer = new Container(BoxLayout.y());
			buttonsContainer.setUIID("DialogBackground");
			buttonsContainer.add(errorLabel).add(okButton);
			dialog.add(buttonsContainer);
			okButton.addActionListener(okAction -> {
				dialog.dispose();
			});

			dialog.setDisposeWhenPointerOutOfBounds(true);
			dialog.show();
		});

		if (users.size() > 0) {
			adminEventTab.add(updateStatusesButton);
		} else {
			addBlankLines(adminEventTab, 1);
			adminEventTab.add(new Label("No contractors interested yet", "WhiteLabelCentered"));
		}

		url = "https://www.google.com/maps/search/" + eventInfo.getAddress().trim() + ", "
				+ eventInfo.getState().trim();
		addressField.addActionListener(action -> {
			url = StringUtil.replaceAll(url, " ", "%20");
			Display.getInstance().execute(url);
		});

		if (status.trim().equals("Available") || status.trim().equals("Denied") || status.trim().equals("")) {
			addBlankLines(eventTab, 2);
			eventTab.add(availableButton);
			addBlankLines(eventTab, 2);
		} else {
			if (status.equals("OSS") || status.equals("Backup") || status.equals("Scriber")
					|| status.equals("Spotter")) {
				addBlankLines(eventTab, 1);
				eventTab.add(new Label("__Manager Info__", "WhiteDarkLabelCentered"));
				addManagerInfo(users, eventTab);
			} else if (status.equals("OSM") || status.equals("AOSM") || status.equals("OSMT")) {
				addBlankLines(eventTab, 1);
				eventTab.add(new Label("__My Roster__", "WhiteDarkLabelCentered"));
				addRoster(users, eventTab);
			}
		}

		eventTab.setUIID("WhiteEventLabel");
		adminEventTab.setUIID("WhiteEventLabel");

		eventTab.setScrollableY(true);
		adminEventTab.setScrollableY(true);

		Container finalContainer = new Container(BoxLayout.y());
		finalContainer.add(headerLabel);
		tabs.addTab("Event Info", eventTab);
		tabs.addTab("Contractor Info", adminEventTab);
		finalContainer.add(tabs);

		this.add(BorderLayout.CENTER, finalContainer);
	}

	private void addUserFields() {
		ArrayList users = eventInfo.getUsers();
		setUsersStatus(users);

		Label ossTimeLabel = new Label("OSS Time: " + eventInfo.getOssStartTime() + " - " + eventInfo.getOssEndTime(),
				"WhiteLabelCentered");
		Label osmTimeLabel = new Label("OSM Time: " + eventInfo.getOsmStartTime() + " - " + eventInfo.getOsmEndTime(),
				"WhiteLabelCentered");
		Label spotterTimeLabel = new Label(
				"Spotter Time: " + eventInfo.getSpotterStartTime() + " - " + eventInfo.getSpotterEndTime(),
				"WhiteLabelCentered");
		Label scriberTimeLabel = new Label(
				"Scriber Time: " + eventInfo.getScriberStartTime() + " - " + eventInfo.getScriberEndTime(),
				"WhiteLabelCentered");
		Label eventDateLabel = new Label(eventInfo.getFormattedEventDate(), "WhiteLabelCentered");

		SpanMultiButton addressField = new SpanMultiButton();
		addressField.setUIID("MapsLinkButtonCentered");
		addressField.setIconUIID("MapsLinkButtonCentered");
		addressField.setUIIDLine1("MapsLinkButtonCentered");
		if (!(status.trim().equals("Available") || status.trim().equals("Denied") || status.trim().equals(""))) {
			addressField.setTextLine1(eventInfo.getAddress() + ",");
			addressField.setTextLine2(eventInfo.getCity() + ", " + eventInfo.getState() + ", " + eventInfo.getZip());
			addressField.setUIIDLine2("MapsLinkButtonCentered");
		} else {
			addressField.setTextLine1(eventInfo.getCity() + ", " + eventInfo.getState() + ", " + eventInfo.getZip());
		}
		TextArea venueLabel = new TextArea(eventInfo.getVenue());

		TextArea notesLabel = new TextArea(eventInfo.getNotes());
		TextArea eventDescLabel = new TextArea(eventInfo.getDescription());
		TextArea publicDescLabel = new TextArea(eventInfo.getPublicDesc());
		TextArea managerNotesLabel = new TextArea(eventInfo.getManagerNote());
		notesLabel.setUIID("WhiteEventLabel");
		eventDescLabel.setUIID("WhiteLabelCentered");
		publicDescLabel.setUIID("WhiteEventLabel");
		managerNotesLabel.setUIID("WhiteEventLabel");
		venueLabel.setUIID("WhiteLabelCentered");
		venueLabel.setEditable(false);
		notesLabel.setEditable(false);
		notesLabel.setFocusable(false);
		eventDescLabel.setEditable(false);
		eventDescLabel.setFocusable(false);
		publicDescLabel.setEditable(false);
		publicDescLabel.setFocusable(false);
		managerNotesLabel.setEditable(false);
		managerNotesLabel.setFocusable(false);

		FontImage eventDateIcon = FontImage.createMaterial(FontImage.MATERIAL_CALENDAR_TODAY, "WhiteEventLabel", 4);
		FontImage venueIcon = FontImage.createMaterial(FontImage.MATERIAL_HOUSE, "WhiteEventLabel", 4);
		FontImage notesIcon = FontImage.createMaterial(FontImage.MATERIAL_NOTES, "WhiteEventLabel", 4);
		FontImage timeIcon = FontImage.createMaterial(FontImage.MATERIAL_TIMER, "WhiteEventLabel", 4);
		FontImage addressIcon = FontImage.createMaterial(FontImage.MATERIAL_LOCATION_PIN, "WhiteEventLabel", 4);
		eventDateLabel.setIcon(eventDateIcon);
		ossTimeLabel.setIcon(timeIcon);
		osmTimeLabel.setIcon(timeIcon);
		spotterTimeLabel.setIcon(timeIcon);
		scriberTimeLabel.setIcon(timeIcon);

		Container c = new Container(BoxLayout.y());

		if (!(status.trim().equals("Available") || status.trim().equals("Denied") || status.trim().equals(""))) {
			Label statusLabel = new Label("You are booked as " + status, "PurpleLabelGoldFontCentered");
			c.add(statusLabel);
			addBlankLines(c, 1);
		}

		////////////////////////
//		Button button = new Button("Form Test");
//		button.setUIID("FatButton");
//		button.addActionListener(event -> {
//			FileSystemStorage fs = FileSystemStorage.getInstance();
//			// Util.copy(Display.getInstance().getResourceAsStream(getClass(), “/abc.pdf”, fs.openOutputStream(fileName));
//			// The above might be a way to copy it to the homePath once downloaded to the phone
////			String fileName = fs.getAppHomePath() + "pdf-sample.pdf";
//			String fileName = "C:\\Users\\tmill\\Downloads\\MILLER.pdf";
//			if (!fs.exists(fileName)) {
//				Util.downloadUrlToFile("http://www.polyu.edu.hk/iaee/files/pdf-sample.pdf", fileName, true);
//			}
//			Display.getInstance().execute(fileName);
//		});
//		c.add(button);
		////////////////////////

		HashMap<String, Object> checkInInfo = getCheckInButtonStatus(status);
		boolean showCheckInButton = (boolean) checkInInfo.get("canCheckIn");
		boolean showCheckOutButton = (boolean) checkInInfo.get("canCheckOut");
		String checkedInTime = (String) checkInInfo.get("checkedInTime");
		String checkedOutTime = (String) checkInInfo.get("checkedOutTime");
		if (checkedOutTime.trim().length() > 0) {
			managerCheckedOutAlready = true;
			Button checkOutButton = new Button("Checked Out Already");
			checkOutButton.setTickerEnabled(false);
			checkOutButton.setUIID("FatButton");
			c.add(checkOutButton);
		} else if (showCheckOutButton) {
			Button checkOutButton = new Button("Check Out");
			checkOutButton.setTickerEnabled(false);
			checkOutButton.setUIID("FatButton");
			checkOutButton.addActionListener(event -> {
				if (sendCheckInOutRequest(false)) {
					managerCheckedOutAlready = true;
					checkOutButton.remove();
					this.repaint();
					this.refreshTheme();
				}
			});
			c.add(checkOutButton);
		} else if (checkedInTime.trim().length() > 0) {
			Button checkOutButton = new Button("Checked In Already");
			checkOutButton.setTickerEnabled(false);
			checkOutButton.setUIID("FatButton");
			c.add(checkOutButton);
		} else if (showCheckInButton) {
			Button checkinButton = new Button("Check In");
			checkinButton.setTickerEnabled(false);
			checkinButton.setUIID("FatButton");
			checkinButton.addActionListener(event -> {
				if (sendCheckInOutRequest(true)) {
					checkinButton.remove();
					this.repaint();
					this.refreshTheme();
				}
			});
			c.add(checkinButton);
		}

		if (!status.trim().equals("Available") && !status.trim().equals("Denied") && !status.trim().equals("")) {
			if (eventDescLabel.getText().trim().length() > 0) {
				c.add(eventDescLabel);
				addBlankLines(c, 1);
			}
		}

		String header = eventInfo.getOrganization();

		if (status.trim().equals("Available") || status.trim().equals("Denied") || status.trim().equals("")) {
			header = "Ace the Event";
		}

		TextArea headerLabel = new TextArea(header);
		if (header.trim().length() > 30) {
			headerLabel.setUIID("OrganizationHeader");
		} else {
			headerLabel.setUIID("OrganizationHeaderLarge");
		}
		headerLabel.setEnabled(false);

		c.add(eventDateLabel);
		addBlankLines(c, 1);

		if (SystemInfo.userInfo.getAccessLevel().equals("M") || SystemInfo.userInfo.getAccessLevel().equals("T")
				|| SystemInfo.userInfo.getAccessLevel().equals("A")) {
			if (eventInfo.getOsmStartTime().trim().length() > 0) {
				c.add(osmTimeLabel);
			}
		}
		if (eventInfo.getOssStartTime().trim().length() > 0) {
			c.add(ossTimeLabel);
		}
		if (eventInfo.getSpotterStartTime().trim().length() > 0) {
			c.add(spotterTimeLabel);
		}
		if (eventInfo.getScriberStartTime().trim().length() > 0) {
			c.add(scriberTimeLabel);
		}
		addBlankLines(c, 1);

		if (!status.trim().equals("Available") && !status.trim().equals("Denied") && !status.trim().equals("")) {
			c.add(venueLabel);
			addBlankLines(c, 1);
		}

		Button availableButton = new Button(userFound ? "Unregister" : "Interested - Deal Me In!");
		availableButton.setTickerEnabled(false);
		availableButton.setUIID(userFound ? "UnregisterButton" : "FatButton");
		availableButton.addActionListener(event -> {

			Dialog dialog = new Dialog(" Which positions are you interested in? ");
			dialog.setUIID("DialogBackground");
			dialog.setDialogUIID("DialogBackground");
			dialog.getTitleComponent().setUIID("StatusDialogLabelUnselected");
			Label ossLabel = new Label("On Site Support", "StatusDialogLabelUnselected");
			Label osmLabel = new Label("On Site Manager", "StatusDialogLabelUnselected");
			Label aosmLabel = new Label("Assistant On Site Manager", "StatusDialogLabelUnselected");
			Label backupLabel = new Label("Backup", "StatusDialogLabelUnselected");
			Label osmtLabel = new Label("On Site Manager Trainee", "StatusDialogLabelUnselected");
			Label spotterLabel = new Label("Spotter", "StatusDialogLabelUnselected");
			Label scriberLabel = new Label("Scriber", "StatusDialogLabelUnselected");

			FontImage ossIcon = FontImage.createMaterial(FontImage.MATERIAL_ADD, "StatusDialogLabelSelected", 6);
			ossLabel.setIcon(ossIcon);
			FontImage osmIcon = FontImage.createMaterial(FontImage.MATERIAL_ADD, "StatusDialogLabelSelected", 6);
			osmLabel.setIcon(osmIcon);
			FontImage aosmIcon = FontImage.createMaterial(FontImage.MATERIAL_ADD, "StatusDialogLabelSelected", 6);
			aosmLabel.setIcon(aosmIcon);
			FontImage backupIcon = FontImage.createMaterial(FontImage.MATERIAL_ADD, "StatusDialogLabelSelected", 6);
			backupLabel.setIcon(backupIcon);
			FontImage osmtIcon = FontImage.createMaterial(FontImage.MATERIAL_ADD, "StatusDialogLabelSelected", 6);
			osmtLabel.setIcon(osmtIcon);
			FontImage spotterIcon = FontImage.createMaterial(FontImage.MATERIAL_ADD, "StatusDialogLabelSelected", 6);
			spotterLabel.setIcon(spotterIcon);
			FontImage scriberIcon = FontImage.createMaterial(FontImage.MATERIAL_ADD, "StatusDialogLabelSelected", 6);
			scriberLabel.setIcon(scriberIcon);

			Label submitButton = new Label("Cancel", "GoldLabelLightBackground");
			ossLabel.addPointerReleasedListener(action -> {
				boolean selected = ossLabel.getUIID().equals("StatusDialogLabelUnselected")
						|| osmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| aosmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| backupLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmtLabel.getUIID().equals("StatusDialogLabelSelected")
						|| spotterLabel.getUIID().equals("StatusDialogLabelSelected")
						|| scriberLabel.getUIID().equals("StatusDialogLabelSelected");
				checkOffLabel(ossLabel, dialog, submitButton, selected);
			});
			osmLabel.addPointerReleasedListener(action -> {
				boolean selected = ossLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmLabel.getUIID().equals("StatusDialogLabelUnselected")
						|| aosmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| backupLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmtLabel.getUIID().equals("StatusDialogLabelSelected")
						|| spotterLabel.getUIID().equals("StatusDialogLabelSelected")
						|| scriberLabel.getUIID().equals("StatusDialogLabelSelected");
				checkOffLabel(osmLabel, dialog, submitButton, selected);
			});
			aosmLabel.addPointerReleasedListener(action -> {
				boolean selected = ossLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| aosmLabel.getUIID().equals("StatusDialogLabelUnselected")
						|| backupLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmtLabel.getUIID().equals("StatusDialogLabelSelected")
						|| spotterLabel.getUIID().equals("StatusDialogLabelSelected")
						|| scriberLabel.getUIID().equals("StatusDialogLabelSelected");
				checkOffLabel(aosmLabel, dialog, submitButton, selected);
			});
			backupLabel.addPointerReleasedListener(action -> {
				boolean selected = ossLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| aosmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| backupLabel.getUIID().equals("StatusDialogLabelUnselected")
						|| osmtLabel.getUIID().equals("StatusDialogLabelSelected")
						|| spotterLabel.getUIID().equals("StatusDialogLabelSelected")
						|| scriberLabel.getUIID().equals("StatusDialogLabelSelected");
				checkOffLabel(backupLabel, dialog, submitButton, selected);
			});
			osmtLabel.addPointerReleasedListener(action -> {
				boolean selected = ossLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| aosmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| backupLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmtLabel.getUIID().equals("StatusDialogLabelUnselected")
						|| spotterLabel.getUIID().equals("StatusDialogLabelSelected")
						|| scriberLabel.getUIID().equals("StatusDialogLabelSelected");
				checkOffLabel(osmtLabel, dialog, submitButton, selected);
			});
			spotterLabel.addPointerReleasedListener(action -> {
				boolean selected = ossLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| aosmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| backupLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmtLabel.getUIID().equals("StatusDialogLabelSelected")
						|| spotterLabel.getUIID().equals("StatusDialogLabelUnselected")
						|| scriberLabel.getUIID().equals("StatusDialogLabelSelected");
				checkOffLabel(spotterLabel, dialog, submitButton, selected);
			});
			scriberLabel.addPointerReleasedListener(action -> {
				boolean selected = ossLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| aosmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| backupLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmtLabel.getUIID().equals("StatusDialogLabelSelected")
						|| spotterLabel.getUIID().equals("StatusDialogLabelSelected")
						|| scriberLabel.getUIID().equals("StatusDialogLabelUnselected");
				checkOffLabel(scriberLabel, dialog, submitButton, selected);
			});

//			Label cancelButton = new Label("Cancel", "GoldLabelLightBackground");

//			Label noFieldsPickedLabel = new Label("      PLEASE SELECT A STATUS      ");
//			noFieldsPickedLabel.setVisible(false);
//			Label spacingLabel = new Label(" ");
//			spacingLabel.setVisible(false);

			submitButton.addPointerReleasedListener(action -> {
				dialog.dispose();
				if (ossLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| aosmLabel.getUIID().equals("StatusDialogLabelSelected")
						|| backupLabel.getUIID().equals("StatusDialogLabelSelected")
						|| osmtLabel.getUIID().equals("StatusDialogLabelSelected")
						|| spotterLabel.getUIID().equals("StatusDialogLabelSelected")
						|| scriberLabel.getUIID().equals("StatusDialogLabelSelected")) {
					setAvailable(!userFound, ossLabel.getUIID().equals("StatusDialogLabelSelected"),
							osmLabel.getUIID().equals("StatusDialogLabelSelected"),
							aosmLabel.getUIID().equals("StatusDialogLabelSelected"),
							backupLabel.getUIID().equals("StatusDialogLabelSelected"),
							osmtLabel.getUIID().equals("StatusDialogLabelSelected"),
							spotterLabel.getUIID().equals("StatusDialogLabelSelected"),
							scriberLabel.getUIID().equals("StatusDialogLabelSelected"));
					if (userFound) {
						availableButton.setText("Interested - Deal Me In!");
					} else {
						availableButton.setText("No Longer Interested");
					}
					availableButton.setUIID(userFound ? "FatButton" : "UnregisterButton");
					userFound = !userFound;
					this.revalidate();
				}
//				else {
////					noFieldsPickedLabel.setVisible(true);
//					spacingLabel.setVisible(true);
//					dialog.revalidate();
//				}
			});
//			cancelButton.addPointerReleasedListener(action -> {
//				dialog.dispose();
//			});

			if (availableButton.getText().equals("Interested - Deal Me In!")) {
				Container positionsContainer = new Container(BoxLayout.y());
				positionsContainer.setUIID("DialogBackground");

				positionsContainer.add(ossLabel);

				// If Admin or Manager, add these options
				if (SystemInfo.userInfo.getAccessLevel().equals("M") || SystemInfo.userInfo.getAccessLevel().equals("T")
						|| SystemInfo.userInfo.getAccessLevel().equals("A")) {
					positionsContainer.add(osmLabel).add(aosmLabel);
					positionsContainer.add(osmtLabel);
				}
				if (SystemInfo.userInfo.getSpotterStatus().equals("true")) {
					positionsContainer.add(spotterLabel);
				}
				if (SystemInfo.userInfo.getScriberStatus().equals("true")) {
					positionsContainer.add(scriberLabel);
				}
				positionsContainer.add(backupLabel);// .add(noFieldsPickedLabel);//.add(spacingLabel).add(noFieldsPickedLabel);

				Container buttonsContainer = new Container(BoxLayout.y());
				buttonsContainer.setUIID("DialogBackground");
				buttonsContainer.add(submitButton);// .add(cancelButton);
				positionsContainer.add(buttonsContainer);
				positionsContainer.add(new Label(
						"                                                                                                                                                          ")); // I
																																														// hate
																																														// this
				dialog.add(positionsContainer);
				dialog.setDisposeWhenPointerOutOfBounds(false);
				dialog.show();
			} else {
				setAvailable(false, false, false, false, false, false, false, false);
				if (userFound) {
					availableButton.setText("Interested - Deal Me In!");
					availableButton.setUIID("FatButton");
				} else {
					availableButton.setUIID("UnregisterButton");
				}
				userFound = !userFound;
				this.revalidate();
			}
		});

		url = "https://www.google.com/maps/search/" + eventInfo.getAddress() + ", " + eventInfo.getCity() + ", "
				+ eventInfo.getState() + ", " + eventInfo.getZip().trim();
		if (!status.trim().equals("Available") && !status.trim().equals("Denied") && !status.trim().equals("")) {
			url = "https://www.google.com/maps/search/" + eventInfo.getCity() + ", " + eventInfo.getState() + ", "
					+ eventInfo.getZip().trim();
		}
		addressField.addActionListener(action -> {
			url = StringUtil.replaceAll(url, " ", "%20");
			Display.getInstance().execute(url);
		});

		if (!status.trim().equals("Available") && !status.trim().equals("Denied") && !status.trim().equals("")) {
			c.add(addressField);

			int screenWidth = Display.getInstance().getDisplayWidth();
			int screenHeight = Display.getInstance().getDisplayHeight();

			int targetWidth = screenWidth / 3;
			int targetHeight = screenHeight / 7;

			try {
//				Image defaultImage = Image.createImage("/checkedHeart.jpg");
				Image defaultImage = Image.createImage("/checkedHeartCentered.jpg");
				Image image = defaultImage.scaled(targetWidth, targetHeight);
				Label star = new Label(image);
				c.add(star);
				addBlankLines(c, 1);
			} catch (Exception e) {
				System.out.println(e);
			}

			if ((status.equals("OSM") || status.equals("AOSM")) && managerNotesLabel.getText().trim().length() > 0) {
				c.add(new Label("__Manager Notes__", "WhiteDarkLabelCentered"));
				c.add(managerNotesLabel);
				addBlankLines(c, 1);
			}
			if (notesLabel.getText().trim().length() > 0) {
				c.add(new Label("__General Notes__", "WhiteDarkLabelCentered"));
				c.add(notesLabel);
				addBlankLines(c, 1);
			}
			if (publicDescLabel.getText().trim().length() > 0) {
				c.add(publicDescLabel);
				addBlankLines(c, 1);
			}
		} else {
			c.add(addressField);
			// Figure out how many OSS, OSM, AOSM, etc are needed.
			int ossNeeded = Integer.parseInt(eventInfo.getNumOssNeeded());
			int aosmNeeded = Integer.parseInt(eventInfo.getNumAosmNeeded());
			int backupNeeded = Integer.parseInt(eventInfo.getNumBackupsNeeded());
			int osmtNeeded = Integer.parseInt(eventInfo.getNumOsmtNeeded());
			int spotterNeeded = Integer.parseInt(eventInfo.getNumSpotterNeeded());
			int scriberNeeded = Integer.parseInt(eventInfo.getNumScriberNeeded());
			int osmNeeded = Integer.parseInt(eventInfo.getNumOsmNeeded());
			for (int i = 0; i < users.size(); i++) {
				UserInfo currUser = new UserInfo((LinkedHashMap) users.get(i));
				if (currUser.getStatus().equals("OSS")) {
					ossNeeded--;
				} else if (currUser.getStatus().equals("AOSM")) {
					aosmNeeded--;
				} else if (currUser.getStatus().equals("Backup")) {
					backupNeeded--;
				} else if (currUser.getStatus().equals("OSMT")) {
					osmtNeeded--;
				} else if (currUser.getStatus().equals("OSM")) {
					osmNeeded--;
				} else if (currUser.getStatus().equals("Spotter")) {
					spotterNeeded--;
				} else if (currUser.getStatus().equals("Scriber")) {
					scriberNeeded--;
				}
			}
			if (osmNeeded > 0) {
				Label totalOsmNeeded = new Label("OSM Needed: " + osmNeeded, "WhiteLabelCentered");
				c.add(totalOsmNeeded);
			}
			if (aosmNeeded > 0) {
				Label totalAosmNeeded = new Label("AOSM Needed: " + aosmNeeded, "WhiteLabelCentered");
				c.add(totalAosmNeeded);
			}
			if (osmtNeeded > 0 && (SystemInfo.userInfo.getAccessLevel().equals("A")
					|| SystemInfo.userInfo.getAccessLevel().equals("T")
					|| SystemInfo.userInfo.getAccessLevel().equals("M"))) {
				Label totalOsmtNeeded = new Label("OSMT Needed: " + osmtNeeded, "WhiteLabelCentered");
				c.add(totalOsmtNeeded);
			}
			if (ossNeeded > 0) {
				Label totalOssNeeded = new Label("OSS Needed: " + ossNeeded, "WhiteLabelCentered");
				c.add(totalOssNeeded);
				if (eventInfo.getOssAmount().trim().length() > 0) {
					Label ossPayLabel = new Label("OSS Event Pay: $" + eventInfo.getOssAmount(), "WhiteLabelCentered");
					c.add(ossPayLabel);
				}
			}
			if (spotterNeeded > 0) {
				Label totalSpotterNeeded = new Label("Spotters Needed: " + spotterNeeded, "WhiteLabelCentered");
				c.add(totalSpotterNeeded);
			}
			if (scriberNeeded > 0) {
				Label totalScriberNeeded = new Label("Scribers Needed: " + scriberNeeded, "WhiteLabelCentered");
				c.add(totalScriberNeeded);
			}
			if (backupNeeded > 0) {
				Label totalBackupNeeded = new Label("Backups Needed: " + backupNeeded, "WhiteLabelCentered");
				c.add(totalBackupNeeded);
			}

			if (osmNeeded <= 0 && aosmNeeded <= 0 && ossNeeded <= 0 && backupNeeded <= 0 && osmtNeeded <= 0
					&& spotterNeeded <= 0 && scriberNeeded <= 0) {
				addBlankLines(c, 1);
				TextArea allBookedLabel = new TextArea(
						"Fully Staffed!\nContinued sign-ups are encouraged to accommodate any potential staffing changes.");
				allBookedLabel.setUIID("WhiteLabelCentered");
				allBookedLabel.setEditable(false);
				c.add(allBookedLabel);
			}
//			addBlankLines(c, 5);			

			int screenWidth = Display.getInstance().getDisplayWidth();
			int screenHeight = Display.getInstance().getDisplayHeight();

			int targetWidth = screenWidth / 3;
			int targetHeight = screenHeight / 7;

			try {
				Image defaultImage = Image.createImage("/checkedHeart.jpg");
				Image image = defaultImage.scaled(targetWidth, targetHeight);
				Label star = new Label(image);
				c.add(star);
				addBlankLines(c, 1);
			} catch (Exception e) {
				System.out.println(e);
			}

			if (publicDescLabel.getText().trim().length() > 0) {
				addBlankLines(c, 1);
				c.add(publicDescLabel);
			}
		}

		Container finalContainer = new Container(BoxLayout.y());
		finalContainer.add(headerLabel);
		addBlankLines(finalContainer, 1);
		finalContainer.add(c);

		if (status.trim().equals("Available") || status.trim().equals("Denied") || status.trim().equals("")) {
			addBlankLines(c, 2);
			finalContainer.add(availableButton);
			addBlankLines(finalContainer, 2);
		} else {
			if (status.equals("OSS") || status.equals("Backup") || status.equals("Spotter")
					|| status.equals("Scriber")) {
				addBlankLines(finalContainer, 1);
				finalContainer.add(new Label("__Manager Info__", "WhiteDarkLabelCentered"));
				addManagerInfo(users, finalContainer);
			} else if (status.equals("OSM") || status.equals("AOSM") || status.equals("OSMT")) {
				addBlankLines(finalContainer, 1);
				finalContainer.add(new Label("__My Roster__", "WhiteDarkLabelCentered"));
				addRoster(users, finalContainer);
			}
		}

		if (SystemInfo.userInfo.getUserName().toLowerCase().equals("tmiller")) {
//			managerCheckedOutAlready = true;
//			addRoster(users, finalContainer);
		}

		finalContainer.setScrollableY(true);

		this.add(BorderLayout.CENTER, finalContainer);
	}

	private void addRoster(ArrayList users, Container c) {
		ArrayList<CheckInRecord> userCheckinTimes = getCheckInTimes();
		ArrayList<UserRating> userRatings = getRatings();
		boolean foundOne = false;
		for (int i = 0; i < users.size(); i++) {
			UserInfo currUser = new UserInfo((LinkedHashMap) users.get(i));
			if (SystemInfo.userInfo.getId() != currUser.getId() && (currUser.getStatus().equals("OSM")
					|| currUser.getStatus().equals("AOSM") || currUser.getStatus().equals("OSS"))) {

				Label contactImage = new Label("", "WhiteLabelCentered");
				try {
					Image defaultImage = Image.createImage("/tmpPhoto.jpg");
//					EncodedImage placeHolder = EncodedImage.createFromImage(defaultImage, true);
					// TODO Handle refreshing this every now and then if it isn't already handling
					// that. New photos would not show because they are cached.
					// C:\Users\tmill\.cn1
//					System.out.println(currUser.getPresignedGetUrl());
//					Image profileImage = URLImage.createCachedImage(currUser.getId() + "_CONTACT_PHOTO_SAVE", url, placeHolder, URLImage.FLAG_RESIZE_SCALE_TO_FILL);
//					Image profileImage = URLImage.createToStorage(placeHolder, currUser.getId() + "_CONTACT_PHOTO_SAVE",
//							currUser.getPresignedGetUrl(), URLImage.RESIZE_SCALE_TO_FILL);
					contactImage.setIcon(defaultImage);

					ConnectionRequest request = new ConnectionRequest(currUser.getPresignedGetUrl(), false) {
						public void postResponse() {
							EncodedImage qr = EncodedImage.create(getResponseData());
							if (EncodedImage.create(getResponseData()).getWidth() > 5) {
								qr.scale(Math.round(Display.getInstance().getDisplayWidth() / 4),
										Math.round(Display.getInstance().getDisplayHeight() / 5));
								contactImage.setIcon(qr);
							}
							revalidate();
						}
					};
					NetworkManager.getInstance().addToQueue(request);

				} catch (IOException e) {
					System.out.println(e);
				}
				contactImage.addPointerReleasedListener(action -> {
					Image img = contactImage.getIcon();
					ImageForm imgForm = new ImageForm(currUser.getFirstName() + " " + currUser.getLastName(), img, this,
							logoutScreen);
					imgForm.show();
				});
				c.add(contactImage);
				c.add(new Label(currUser.getFirstName() + " " + currUser.getLastName(), "WhiteLabelCentered"));
				Label phoneLabel = new Label(currUser.getPhone(), "MapsLinkButtonCentered");
				phoneLabel.addPointerReleasedListener(e -> {
					Display.getInstance().dial(phoneLabel.getText());
				});
				c.add(phoneLabel);
				if (currUser.getEmail().trim().length() > 0) {
					Label emailLabel = new Label(currUser.getEmail(), "WhiteLabelCentered");
					c.add(emailLabel);
				}
				c.add(new Label(currUser.getStatus(), "WhiteLabelCentered"));
				c.add(new Label(""));
				if (managerCheckedOutAlready) {
//					Button checkInTimesButton = new Button("Confirm Shifts");
//					checkInTimesButton.setTickerEnabled(false);
//					checkInTimesButton.setUIID("FatButton");

					userEnteredCheckInTime = eventInfo.getOssStartTime();
					userEnteredCheckOutTime = eventInfo.getOssEndTime();
					boolean userCheckedIn = false;
					boolean userCheckedOut = false;
					for (int u = 0; u < userCheckinTimes.size(); u++) {
						CheckInRecord currCheckInRecord = userCheckinTimes.get(u);
						if (currCheckInRecord.getUserId().equals(currUser.getId() + "")) {
							if (currCheckInRecord.getCheckInTime().trim().length() > 0) {
								userCheckedIn = true;
							}
							if (currCheckInRecord.getCheckOutTime().trim().length() > 0) {
								userCheckedOut = true;
							}
							userEnteredCheckInTime = currCheckInRecord.getCheckInTime();
							userEnteredCheckOutTime = currCheckInRecord.getCheckOutTime();
						}
					}

//					Dialog dialog = new Dialog("Edit Times");

//					dialog.setUIID("UserStatesBackground");
//					dialog.setDialogUIID("UserStatesBackground");
//					dialog.getTitleComponent().setUIID("GoldLabelLightBackground");

					Label checkInTimeLabel = new Label("    Check In Time    ", "WhiteLabelCentered");
//					TextField checkInTimeField = new TextField(userEnteredCheckInTime);
					Label checkOutTimeLabel = new Label("    Check Out Time    ", "WhiteLabelCentered");
//					TextField checkOutTimeField = new TextField(userEnteredCheckOutTime);

					Picker checkInTimeField = new Picker();
					checkInTimeField.setStrings(generateTimeArray());

					Picker checkOutTimeField = new Picker();
					checkOutTimeField.setStrings(generateTimeArray());

					if (userEnteredCheckInTime.trim().length() > 0) {
						checkInTimeField.setText(userEnteredCheckInTime);
					}

					if (userEnteredCheckOutTime.trim().length() > 0) {
						checkOutTimeField.setText(userEnteredCheckOutTime);
					}

					Label submitLabel = new Label("Submit Times", "WhiteLabelCenteredBold");
					submitLabel.addPointerReleasedListener(submitAction -> {
						submitNewTimes(checkInTimeField.getText(), checkOutTimeField.getText(), currUser.getId(),
								eventInfo.getId());
//						dialog.dispose();
					});
//					Label cancelLabel = new Label("Cancel", "UserStatesBackground");
//					cancelLabel.addPointerReleasedListener(submitAction -> {
//						dialog.dispose();
//					});

//					Container buttonsContainer = new Container(BoxLayout.y());
//					buttonsContainer.setUIID("UserStatesBackground");

					if (!userCheckedIn || !userCheckedOut) {
						String labelMsg = "";
						if (!userCheckedIn && !userCheckedOut) {
							labelMsg = "    User did not check in or out    ";
						} else if (!userCheckedIn) {
							labelMsg = "    User did not check in    ";
						} else {
							labelMsg = "    User did not check out    ";
						}
						Label noTimesFoundLabel = new Label(labelMsg, "WhiteLabelCenteredBold");
						c.add(noTimesFoundLabel);
					}

					c.add(checkInTimeLabel).add(checkInTimeField).add(checkOutTimeLabel).add(checkOutTimeField)
							.add(submitLabel);// .add(cancelLabel);

//					dialog.add(buttonsContainer);
//					dialog.setDisposeWhenPointerOutOfBounds(true);
//					checkInTimesButton.addActionListener(action -> {
//						dialog.show();
//					});
//					c.add(checkInTimesButton);

					String userRating = "";
					for (int u = 0; u < userRatings.size(); u++) {
						UserRating currUserRating = userRatings.get(u);
						if (currUserRating.getUserId().equals(currUser.getId() + "")) {
							userRating = currUserRating.getRating();
						}
					}

					Button rateButton = new Button(
							userRating.trim().length() > 0 ? "Rating Completed" : "Rate " + currUser.getFirstName());
					rateButton.setTickerEnabled(false);
					rateButton.setUIID("FatButton");

					Dialog rateDialog = new Dialog("Rate " + currUser.getFirstName());
					rateDialog.setUIID("HeaderBackground");
					rateDialog.setDialogUIID("HeaderBackground");
					rateDialog.getTitleComponent().setUIID("Label");
					rateDialog.setDisposeWhenPointerOutOfBounds(true);
					Container allFieldsContainer = new Container(BoxLayout.y());
					Container ratingContainer = new Container(BoxLayout.x());
					ratingContainer.setUIID("HeaderBackground");

//					Button submitRatingButton = new Button("Submit");
//					try {
//						int screenWidth = Display.getInstance().getDisplayWidth();
//						int screenHeight = Display.getInstance().getDisplayHeight();
//
//						int targetWidth = screenWidth / 8;
//						int targetHeight = screenHeight * 1 / 10; // 30% of the screen height
//
//						EncodedImage goldStarImage = EncodedImage.create("/goldStar.png");
//						EncodedImage emptyStarImage = EncodedImage.create("/emptyStar.jpg");
//						Image image1 = goldStarImage.scaled(targetWidth, targetHeight);
//						Image image2 = emptyStarImage.scaled(targetWidth, targetHeight);
//
//						Label star1 = new Label(image1);
//						Label star2 = new Label(image2);
//						Label star3 = new Label(image2);
//						Label star4 = new Label(image2);
//						Label star5 = new Label(image2);
//						
//						if (userRating.equals("5")) {
//							star1.setIcon(image1);
//							star2.setIcon(image1);
//							star3.setIcon(image1);
//							star4.setIcon(image1);
//							star5.setIcon(image1);
//						}else if (userRating.equals("4")) {
//							star1.setIcon(image1);
//							star2.setIcon(image1);
//							star3.setIcon(image1);
//							star4.setIcon(image1);
//							star5.setIcon(image2);
//						}else if (userRating.equals("3")) {
//							star1.setIcon(image1);
//							star2.setIcon(image1);
//							star3.setIcon(image1);
//							star4.setIcon(image2);
//							star5.setIcon(image2);
//						}else if (userRating.equals("2")) {
//							star1.setIcon(image1);
//							star2.setIcon(image1);
//							star3.setIcon(image2);
//							star4.setIcon(image2);
//							star5.setIcon(image2);
//						}
//
//						star1.addPointerReleasedListener(action -> {
//							star1.setIcon(image1);
//							star2.setIcon(image2);
//							star3.setIcon(image2);
//							star4.setIcon(image2);
//							star5.setIcon(image2);
//							c.repaint();
//							c.revalidate();
//						});
//
//						star2.addPointerReleasedListener(action -> {
//							star1.setIcon(image1);
//							star2.setIcon(image1);
//							star3.setIcon(image2);
//							star4.setIcon(image2);
//							star5.setIcon(image2);
//							c.repaint();
//							c.revalidate();
//						});
//
//						star3.addPointerReleasedListener(action -> {
//							star1.setIcon(image1);
//							star2.setIcon(image1);
//							star3.setIcon(image1);
//							star4.setIcon(image2);
//							star5.setIcon(image2);
//							c.repaint();
//							c.revalidate();
//						});
//
//						star4.addPointerReleasedListener(action -> {
//							star1.setIcon(image1);
//							star2.setIcon(image1);
//							star3.setIcon(image1);
//							star4.setIcon(image1);
//							star5.setIcon(image2);
//							c.repaint();
//							c.revalidate();
//						});
//
//						star5.addPointerReleasedListener(action -> {
//							star1.setIcon(image1);
//							star2.setIcon(image1);
//							star3.setIcon(image1);
//							star4.setIcon(image1);
//							star5.setIcon(image1);
//							c.repaint();
//							c.revalidate();
//						});
//
//						ratingContainer.add(star1).add(star2).add(star3).add(star4).add(star5);
//						submitRatingButton.setUIID("FatButton");
//						submitRatingButton.addActionListener(action -> {
//							submitRating(star1.getIcon().equals(image1), star2.getIcon().equals(image1),
//									star3.getIcon().equals(image1), star4.getIcon().equals(image1),
//									star5.getIcon().equals(image1), currUser.getId(), eventInfo.getId());
//							rateButton.setText("Rating Completed");
//							rateDialog.dispose();
//						});
//					} catch (Exception e) {
//						System.out.println(e);
//					}
					rateButton.addActionListener(event -> {
						rateDialog.show();
					});
					allFieldsContainer.add(ratingContainer);
					allFieldsContainer.add(new Label(" ", "Label"));
//					allFieldsContainer.add(submitRatingButton);
					allFieldsContainer.add(new Label(" ", "Label"));
					allFieldsContainer.add(new Label(" ", "Label"));
					rateDialog.add(allFieldsContainer);

					///////////
					Container ratingContainer2 = new Container(BoxLayout.x());

					ratingContainer.setUIID("WhiteLabelCentered");
					ratingContainer2.setUIID("WhiteLabelCentered");
					try {
						int screenWidth = Display.getInstance().getDisplayWidth();
						int screenHeight = Display.getInstance().getDisplayHeight();

						int targetWidth = screenWidth / 5;
						int targetHeight = screenHeight / 10; // 8% of the screen height

//						Image defaultImage = Image.createImage("/checkedHeart.jpg");
//						Image defaultImage2 = Image.createImage("/uncheckedHeart.jpg");
						Image defaultImage = Image.createImage("/checkedHeartCentered.jpg");
						Image defaultImage2 = Image.createImage("/uncheckedHeart.jpg");

//						Image image1 = defaultImage.scaled(targetWidth, targetHeight);
//						Image image2 = defaultImage2.scaled(targetWidth, targetHeight);
						Image image1 = defaultImage.scaledWidth(targetWidth);
						Image image2 = defaultImage2.scaledWidth(targetWidth);

						Label star1 = new Label(image1);
						Label star2 = new Label(image2);
						Label star3 = new Label(image2);
						Label star4 = new Label(image2);
						Label star5 = new Label(image2);

						if (userRating.equals("5")) {
							star1.setIcon(image1);
							star2.setIcon(image1);
							star3.setIcon(image1);
							star4.setIcon(image1);
							star5.setIcon(image1);
						} else if (userRating.equals("4")) {
							star1.setIcon(image1);
							star2.setIcon(image1);
							star3.setIcon(image1);
							star4.setIcon(image1);
							star5.setIcon(image2);
						} else if (userRating.equals("3")) {
							star1.setIcon(image1);
							star2.setIcon(image1);
							star3.setIcon(image1);
							star4.setIcon(image2);
							star5.setIcon(image2);
						} else if (userRating.equals("2")) {
							star1.setIcon(image1);
							star2.setIcon(image1);
							star3.setIcon(image2);
							star4.setIcon(image2);
							star5.setIcon(image2);
						}

						if (userRating.trim().length() == 0) {
							star1.addPointerReleasedListener(action -> {
								star1.setIcon(image1);
								star2.setIcon(image2);
								star3.setIcon(image2);
								star4.setIcon(image2);
								star5.setIcon(image2);
								c.repaint();
								c.revalidate();
							});

							star2.addPointerReleasedListener(action -> {
								star1.setIcon(image1);
								star2.setIcon(image1);
								star3.setIcon(image2);
								star4.setIcon(image2);
								star5.setIcon(image2);
								c.repaint();
								c.revalidate();
							});

							star3.addPointerReleasedListener(action -> {
								star1.setIcon(image1);
								star2.setIcon(image1);
								star3.setIcon(image1);
								star4.setIcon(image2);
								star5.setIcon(image2);
								c.repaint();
								c.revalidate();
							});

							star4.addPointerReleasedListener(action -> {
								star1.setIcon(image1);
								star2.setIcon(image1);
								star3.setIcon(image1);
								star4.setIcon(image1);
								star5.setIcon(image2);
								c.repaint();
								c.revalidate();
							});

							star5.addPointerReleasedListener(action -> {
								star1.setIcon(image1);
								star2.setIcon(image1);
								star3.setIcon(image1);
								star4.setIcon(image1);
								star5.setIcon(image1);
								c.repaint();
								c.revalidate();
							});
						}

						star1.setUIID("starRating");
						star2.setUIID("starRating");
						star3.setUIID("starRating");
						star4.setUIID("starRating");
						star5.setUIID("starRating");
						ratingContainer2.add(star1).add(star2).add(star3).add(star4).add(star5);
						c.add(new Label(" ", "WhiteEventLabel"));
						c.add(ratingContainer2);
						Label submitRatingLabelButton = new Label(
								userRating.trim().length() > 0 ? "Rating Submitted" : "Click to Submit Rating",
								"WhiteLabelCenteredBold");
						submitRatingLabelButton.addPointerReleasedListener(action -> {
							if (!submitRatingLabelButton.getText().equals("Rating Submitted")) {
								submitRating(star1.getIcon().equals(image1), star2.getIcon().equals(image1),
										star3.getIcon().equals(image1), star4.getIcon().equals(image1),
										star5.getIcon().equals(image1), currUser.getId(), eventInfo.getId());
								submitRatingLabelButton.setText("Rating Submitted");
								star1.removePointerReleasedListener(removed -> {
								});
								star2.removePointerReleasedListener(removed -> {
								});
								star3.removePointerReleasedListener(removed -> {
								});
								star4.removePointerReleasedListener(removed -> {
								});
								star5.removePointerReleasedListener(removed -> {
								});

								c.repaint();
								c.revalidate();
							}
						});
						c.add(new Label(" ", "WhiteEventLabel"));
						c.add(submitRatingLabelButton);
						c.add(new Label(" ", "WhiteEventLabel"));
					} catch (Exception e) {
						System.out.println(e);
					}
				}
				c.revalidate();

				// Give it time to load the image to the form correctly
				try {
					Thread.sleep(3);
				} catch (Exception e) {

				}
				foundOne = true;
				c.add(new Label(" "));
			}
		}
		if (!foundOne) {
			c.add(new Label("No one assigned yet", "WhiteLabelCentered"));
		}
		c.add(new Label(" ", "WhiteLabelCentered"));
	}

	private String[] generateTimeArray() {
		String[] timeArray = new String[81]; // Total 288 slots for 12 hours * 60 minutes / 5 minute increments
		int index = 1;
		int hour = 4; // Starting hour (12 AM is 0 in 24-hour format)
		timeArray[0] = "";

		while (hour < 24) { // Loop through 24 hours
			for (int minute = 0; minute < 60; minute += 15) { // Increment minutes by 10
				timeArray[index++] = formatTime(hour, minute); // Format and add to the array
			}
			hour++;
		}

		return timeArray; // Return the populated array
	}

	// Helper method to format the time into a 12-hour format with AM/PM
	private String formatTime(int hour, int minute) {
		String ampm = hour < 12 ? "AM" : "PM";
		int displayHour = hour % 12; // Convert to 12-hour format
		if (displayHour == 0)
			displayHour = 12; // Handle the case for 12 AM/PM
		String formattedMinute = (minute < 10) ? "0" + minute : String.valueOf(minute); // Pad minutes with a leading
																						// zero if needed

		return displayHour + ":" + formattedMinute + " " + ampm;
	}

	private int[] parseTime(String time) {
		// Split the time into hours, minutes, and AM/PM parts using StringUtil
		List<String> timeParts = StringUtil.tokenize(time, " ");
		List<String> hm = StringUtil.tokenize(timeParts.get(0), ":");

		int hour = Integer.parseInt(hm.get(0));
		int minute = Integer.parseInt(hm.get(1));
		String ampm = timeParts.get(1);

		// Convert to 24-hour format if necessary
		if (ampm.equalsIgnoreCase("PM") && hour != 12) {
			hour += 12;
		} else if (ampm.equalsIgnoreCase("AM") && hour == 12) {
			hour = 0; // Handle 12:00 AM as midnight
		}

		return new int[] { hour, minute }; // Return hour and minute as an int array
	}

	private void submitNewTimes(String checkInTime, String checkOutTime, int userId, int eventId) {
		BackendCall submitTimesReq = new BackendCall("submitNewCheckInTimes", true);
		HashMap<String, Object> args = new HashMap();
		args.put("checkInTime", checkInTime);
		args.put("checkOutTime", checkOutTime);
		args.put("userId", userId);
		args.put("eventId", eventId);
		Map<String, Object> result = submitTimesReq.makeCall(args);
		System.out.println(result);
	}

	private void submitRating(boolean oneStar, boolean twoStar, boolean threeStar, boolean fourStar, boolean fiveStar,
			int userId, int eventId) {
		BackendCall submitTimesReq = new BackendCall("submitUserRating", true);
		HashMap<String, Object> args = new HashMap();
		String rating = "";
		if (fiveStar) {
			rating = "5";
		} else if (fourStar) {
			rating = "4";
		} else if (threeStar) {
			rating = "3";
		} else if (twoStar) {
			rating = "2";
		} else {
			rating = "1";
		}
		args.put("rating", rating);
		args.put("userId", userId);
		args.put("eventId", eventId);
		args.put("raterId", SystemInfo.userInfo.getId() + "");
		Map<String, Object> result = submitTimesReq.makeCall(args);
		System.out.println(result);
	}

	private ArrayList<CheckInRecord> getCheckInTimes() {
		BackendCall checkInTimesReq = new BackendCall("getUserCheckInInfo", false);
		HashMap<String, Object> args = new HashMap();
		args.put("eventId", (int) eventInfo.getId() + "");
		Map<String, Object> result = checkInTimesReq.makeCall(args);
		HashMap<String, Object> results = new HashMap();

		ArrayList<CheckInRecord> cirs = new ArrayList();
		ArrayList list = (ArrayList) result.get("root");
		for (int i = 0; i < list.size(); i++) {
			CheckInRecord checkInRecord = new CheckInRecord((LinkedHashMap) list.get(i));
			cirs.add(checkInRecord);
		}
		return cirs;
	}

	private ArrayList<UserRating> getRatings() {
		BackendCall checkInTimesReq = new BackendCall("getUserRatingInfo", false);
		HashMap<String, Object> args = new HashMap();
		args.put("eventId", (int) eventInfo.getId() + "");
		args.put("raterId", SystemInfo.userInfo.getId() + "");
		Map<String, Object> result = checkInTimesReq.makeCall(args);
		HashMap<String, Object> results = new HashMap();

		ArrayList<UserRating> userRatings = new ArrayList();
		ArrayList list = (ArrayList) result.get("root");
		for (int i = 0; i < list.size(); i++) {
			UserRating userRating = new UserRating((LinkedHashMap) list.get(i));
			userRatings.add(userRating);
		}
		return userRatings;
	}

	private void addManagerInfo(ArrayList users, Container c) {
		boolean foundOne = false;
		for (int i = 0; i < users.size(); i++) {
			UserInfo currUser = new UserInfo((LinkedHashMap) users.get(i));
			if (SystemInfo.userInfo.getId() != currUser.getId() && (currUser.getStatus().equals("OSM"))) {
				Label contactImage = new Label("", "WhiteLabelCentered");
				try {
					Image defaultImage = Image.createImage("/tmpPhoto.jpg");
//					EncodedImage placeHolder = EncodedImage.createFromImage(defaultImage, true);
					// TODO Handle refreshing this every now and then if it isn't already handling
					// that. New photos would not show because they are cached.
//					Image profileImage = URLImage.createToStorage(placeHolder, currUser.getId() + "_CONTACT_PHOTO_SAVE",
//							currUser.getPresignedGetUrl(), URLImage.RESIZE_SCALE_TO_FILL);
					contactImage.setIcon(defaultImage);

					ConnectionRequest request = new ConnectionRequest(currUser.getPresignedGetUrl(), false) {
						public void postResponse() {
							EncodedImage qr = EncodedImage.create(getResponseData());
							if (EncodedImage.create(getResponseData()).getWidth() > 5) {
								qr.scale(Math.round(Display.getInstance().getDisplayWidth() / 4),
										Math.round(Display.getInstance().getDisplayHeight() / 5));
								contactImage.setIcon(qr);
							}
							revalidate();
						}
					};
					NetworkManager.getInstance().addToQueue(request);
				} catch (IOException e) {
					System.out.println(e);
				}
				contactImage.addPointerReleasedListener(action -> {
					Image img = contactImage.getIcon();
					ImageForm imgForm = new ImageForm(currUser.getFirstName() + " " + currUser.getLastName(), img, this,
							logoutScreen);
					imgForm.show();
				});
				c.add(contactImage);
				c.add(new Label(currUser.getFirstName() + " " + currUser.getLastName(), "WhiteLabelCentered"));
				Label phoneLabel = new Label(currUser.getPhone(), "MapsLinkButtonCentered");
				phoneLabel.addPointerReleasedListener(e -> {
					Display.getInstance().dial(phoneLabel.getText());
				});
				c.add(phoneLabel);
				c.add(new Label(currUser.getStatus(), "WhiteLabelCentered"));
				c.revalidate();

				// Give it time to load the image to the form correctly
				try {
					Thread.sleep(5);
				} catch (Exception e) {

				}
				foundOne = true;
			}
		}
		if (!foundOne) {
			c.add(new Label("No manager assigned yet", "WhiteLabelCentered"));
		}
		c.add(new Label(" ", "WhiteLabelCentered"));
	}

	private void setUsersStatus(ArrayList users) {
		for (int i = 0; i < users.size(); i++) {
			UserInfo currUser = new UserInfo((LinkedHashMap) users.get(i));
			if (SystemInfo.userInfo.getId() == currUser.getId()) {
				status = currUser.getStatus();
				userFound = true;
			}
		}
	}

	private void setAvailable(boolean isAvailable, boolean oss, boolean osm, boolean aosm, boolean backup, boolean osmt,
			boolean spotter, boolean scriber) {
		BackendCall loginReq = new BackendCall("setAvailable", true);
		HashMap<String, Object> args = new HashMap();
		args.put("isAvailable", isAvailable ? "True" : "False");
		args.put("userId", (int) SystemInfo.userInfo.getId() + "");
		args.put("eventId", (int) eventInfo.getId() + "");
		args.put("oss", oss + "");
		args.put("osm", osm + "");
		args.put("aosm", aosm + "");
		args.put("backup", backup + "");
		args.put("osmt", osmt + "");
		args.put("spotter", spotter + "");
		args.put("scriber", scriber + "");
		Map<String, Object> result = loginReq.makeCall(args);

		this.repaint();
		this.refreshTheme();
	}

	private boolean sendCheckInOutRequest(boolean isCheckIn) {
		Calendar calendar = Calendar.getInstance();
		int hours = calendar.get(Calendar.HOUR_OF_DAY);
		int minutes = calendar.get(Calendar.MINUTE);
		boolean addZeros = false;
		if (minutes < 10) {
			addZeros = true;
		}

		BackendCall checkInStatusReq = new BackendCall(isCheckIn ? "checkIn" : "checkOut", true);
		HashMap<String, Object> args = new HashMap();
		args.put("userId", (int) SystemInfo.userInfo.getId() + "");
		args.put("eventId", (int) eventInfo.getId() + "");
		args.put("time", hours + ":" + (addZeros ? "0" : "") + minutes);
		Map<String, Object> result = checkInStatusReq.makeCall(args);
		HashMap<String, Object> results = new HashMap();
		return result.get("result").equals("Success");
	}

	// TODO If they dont have location on then we need to let them know.
	private HashMap<String, Object> getCheckInButtonStatus(String status) {
//		double latitude = 0;
//		double longitude = 0;
//		try {
//			LocationManager locationManager = LocationManager.getLocationManager();
//
//			if (locationManager != null && locationManager.isGPSDetectionSupported()) {
//				Location location = locationManager.getCurrentLocation();
//
//				if (location != null) {
//					latitude = location.getLatitude();
//					longitude = location.getLongitude();
//
//					System.out.println("Current location: " + latitude + ", " + longitude);
//				} else {
//					System.out.println("Location data not available");
//				}
//			} else {
//				System.out.println("GPS detection is not supported");
//			}
//		} catch (Exception e) {
//			System.out.println(e);
//		}

		Calendar calendar = Calendar.getInstance();
		int hours = calendar.get(Calendar.HOUR_OF_DAY);
		int minutes = calendar.get(Calendar.MINUTE);
		boolean addZeros = false;
		if (minutes < 10) {
			addZeros = true;
		}

		BackendCall checkInStatusReq = new BackendCall("checkIfCanCheckIn", false);
		HashMap<String, Object> args = new HashMap();
		args.put("userId", (int) SystemInfo.userInfo.getId() + "");
		args.put("eventId", (int) eventInfo.getId() + "");
//		args.put("locationValue", latitude + "|" + longitude);
		args.put("time", hours + ":" + (addZeros ? "0" : "") + minutes);
		args.put("status", status);
		Map<String, Object> result = checkInStatusReq.makeCall(args);
		HashMap<String, Object> results = new HashMap();
		results.put("canCheckIn", result.get("canCheckIn").equals("true"));
		results.put("canCheckOut", result.get("canCheckOut").equals("true"));
		results.put("checkedInTime", result.get("checkedInTime"));
		results.put("checkedOutTime", result.get("checkedOutTime"));
		return results;
	}

	// TODO Make this bulk update rather than individual
	private String setStatus(int userId, String status) {
		BackendCall loginReq = new BackendCall("setStatus", true);
		HashMap<String, Object> args = new HashMap();
		args.put("status", status);
		args.put("userId", userId + "");
		args.put("eventId", eventInfo.getId());
		Map<String, Object> result = loginReq.makeCall(args);

		if (((ArrayList) result.get("root")).size() > 0) {
			return (String) (((ArrayList) result.get("root")).get(0));
		}

		this.repaint();
		this.refreshTheme();
		return "";
	}

	private String[] getStatuses(UserInfo user) {
		ArrayList<String> statuses = new ArrayList();
		if (user.getOssStatus().equals("true")) {
			statuses.add("OSS");
		}
		if (user.getOsmStatus().equals("true")) {
			statuses.add("OSM");
		}
		if (user.getAosmStatus().equals("true")) {
			statuses.add("AOSM");
		}
		if (user.getBackupStatus().equals("true")) {
			statuses.add("Backup");
		}
		if (user.getOsmtStatus().equals("true")) {
			statuses.add("Osmt");
		}
		if (user.getSpotterStatus().equals("true")) {
			statuses.add("Spotter");
		}
		if (user.getScriberStatus().equals("true")) {
			statuses.add("Scriber");
		}
		statuses.add("Available");
		statuses.add("Denied");

		return statuses.toArray(new String[statuses.size()]);
	}

	private void addBlankLines(Container c, int numRows) {
		while (numRows-- > 0) {
			c.add(new Label(" ", "EventContainer"));
		}
	}

	private void checkOffLabel(Label label, Dialog dialog, Label submitCancelLabel, boolean selected) {
		FontImage icon = FontImage.createMaterial(FontImage.MATERIAL_REMOVE, "StatusDialogLabelSelected", 6);
		;
		if (label.getUIID().equals("StatusDialogLabelUnselected")) {
			label.setUIID("StatusDialogLabelSelected");
		} else {
			label.setUIID("StatusDialogLabelUnselected");
			icon = FontImage.createMaterial(FontImage.MATERIAL_ADD, "StatusDialogLabelSelected", 6);
		}
		label.setIcon(icon);
		submitCancelLabel.setText(selected ? "Submit" : "Cancel");
		dialog.revalidate();
	}
}