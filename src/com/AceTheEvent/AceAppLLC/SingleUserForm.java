package com.AceTheEvent.AceAppLLC;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.codename1.ui.Button;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.layouts.BoxLayout;

public class SingleUserForm extends Form {

	private UserInfo userInfo = null;

	public SingleUserForm(Form backForm, UserInfo userInfo) {
		super("", BoxLayout.y());
//		this.setSafeArea(true);
		this.userInfo = userInfo;

		Button backButton = new Button("Back");
		backButton.setTickerEnabled(false);
		backButton.addActionListener(event -> {
			backForm.show();
		});
		this.add(backButton);

		addFields();
	}

	private void addFields() {
		Label nameLabel = new Label("Name");
		Label nameField = new Label(userInfo.getUserName());
		
		Button deleteButton = new Button("Delete User");
		deleteButton.setTickerEnabled(false);
		deleteButton.addActionListener(action ->{
			deleteUser(userInfo.getId());
		});

		this.add(nameLabel);
		this.add(nameField);
		this.add(deleteButton);
	}

	private void deleteUser(int deletingUserId) {
		BackendCall loginReq = new BackendCall("deleteUser", true);
		HashMap<String, Object> args = new HashMap();
		args.put("userId", deletingUserId);
		Map<String, Object> result = loginReq.makeCall(args);

		AllUsersForm allUsersForm = new AllUsersForm(this);
		allUsersForm.show();
	}
}
