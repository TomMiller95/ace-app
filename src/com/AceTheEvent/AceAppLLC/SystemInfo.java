package com.AceTheEvent.AceAppLLC;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;
import java.util.StringTokenizer;

import com.codename1.io.FileSystemStorage;
import com.codename1.io.JSONParser;
import com.codename1.io.MultipartRequest;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Display;
import com.codename1.ui.Image;

public class SystemInfo {

	public static UserInfo userInfo = null;
	public static String authToken = "";
	public static String version = "2.0";

	public static String formatDate(String unformattedDate) {
		String formattedDate = "";
		String[] chunks = split(unformattedDate, "/");
		formattedDate = "20" + chunks[2] + "-" + chunks[0] + "-" + chunks[1];
		return formattedDate;
	}

	public static String[] split(String str, String splitBy) {
		ArrayList<String> splitArray = new ArrayList<>();
		StringTokenizer arr = new StringTokenizer(str, splitBy);
		while (arr.hasMoreTokens())
			splitArray.add(arr.nextToken());
		return splitArray.toArray(new String[splitArray.size()]);
	}

	public static void uploadToS3(String url, String filePath, boolean isPNG, Image img)
			throws UnsupportedEncodingException, IOException {

//		EncodedImage em = EncodedImage.createFromImage(img, false);
//		byte[] data = em.getImageData();
//		
//		ConnectionRequest r = new ConnectionRequest();
//		r.setUrl(url);
//		r.setHttpMethod("PUT");
//		r.addArgument("file", data);
//		r.addArgument("body", data);
//		r.addRequestHeader("Content-Type", "image/jpg");
//		r.addArgument("fileUpload", data);
//
//		NetworkManager.getInstance().addToQueueAndWait(r);
//		Map<String, Object> result = new JSONParser()
//				.parseJSON(new InputStreamReader(new ByteArrayInputStream(r.getResponseData()), "UTF-8"));
//		System.out.println(result);

		/////////////////////////////

		MultipartRequest request = new MultipartRequest() {
			protected void readResponse(InputStream input) throws IOException {
				JSONParser jp = new JSONParser();
				Map<String, Object> result = jp.parseJSON(new InputStreamReader(input, "UTF-8"));
				String url = (String) result.get("url");
				if (url == null) {
					return;
				}
			}
		};

		request.setUrl(
				isPNG ? SystemInfo.userInfo.getPresignedPutUrlPNG() : SystemInfo.userInfo.getPresignedPutUrlJPEG());
		try {

			Display.getInstance().openGallery((e) -> {
				if (e != null && e.getSource() != null) {
					String path = (String) e.getSource();

					request.setHttpMethod("PUT");
					request.setReadResponseForErrors(true);
					request.addArgument("ContentType","image/png");
					request.addArgument("ACL","public-read");
					request.addArgument("ContentEncoding","base64");
					try {
//						request.addData("file", path, "image/" + (isPNG ? "png" : "jpeg"));
//						request.addData("file", path, "");
						request.addData("body", path, "image/png");
						NetworkManager.getInstance().addToQueue(request);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}, Display.GALLERY_IMAGE);

		} catch (Exception err) {
			err.printStackTrace();
		}
	}
	
	public static String getVersion() {
		return version;
	}
}