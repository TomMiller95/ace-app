package com.AceTheEvent.AceAppLLC;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.codename1.components.FloatingActionButton;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.CheckBox;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.SwipeableContainer;
import com.codename1.ui.URLImage;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.SwipeBackSupport;

public class UserForm extends Form {

	private boolean madeChange = false;
	private ArrayList<String> interestedStates = new ArrayList();
	private UserInfo userInfo = null;
	private SideMenuButton menuButton = null;

	public UserForm(Form backForm, Form logoutScreen) {
		super("My Profile", new BorderLayout());

		this.setUIID("background");

		FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_ARROW_BACK);
		fab.addActionListener(e -> {
			menuButton.loadAllEvents();
		});
		fab.setUIID("Fab");
		fab.bindFabToContainer(this.getContentPane(), Component.LEFT, Component.BOTTOM);

		menuButton = new SideMenuButton(this, backForm, logoutScreen, fab, true);

		Button showStatesButton = new Button("Update Service Area");
		showStatesButton.setTickerEnabled(false);
		showStatesButton.setUIID("FatButtonLight");
		showStatesButton.addActionListener(event -> {
			UserStatesForm usf = new UserStatesForm(this, logoutScreen);
			usf.show();
		});

		userInfo = SystemInfo.userInfo;

		Label nameLabel = new Label(userInfo.getFirstName() + " " + userInfo.getLastName(), "UserProfileLabel");
		Label emailLabel = new Label(userInfo.getEmail(), "UserProfileLabel");
		Label cityStZipLabel = new Label(userInfo.getCity() + ", " + userInfo.getState() + ", " + userInfo.getZip(),
				"UserProfileLabel");
		Label addressLabel = new Label(userInfo.getAddress(), "UserProfileLabel");
		Label phoneLabel = new Label(userInfo.getPhone(), "UserProfileLabel");
		Label contactImage = new Label("", "UserProfilePhotoLabel");
		try {
			Image defaultImage = Image.createImage("/tmpPhoto.jpg");
//			EncodedImage placeHolder = EncodedImage.createFromImage(defaultImage, true);
			// TODO Handle refreshing this every now and then if it isn't already handling
			// that. New photos would not show because they are cached.
//			Image profileImage = URLImage.createToStorage(placeHolder, userInfo.getId() + "_CONTACT_PHOTO_SAVE",
//					userInfo.getPresignedGetUrl(), URLImage.RESIZE_SCALE_TO_FILL);
			contactImage.setIcon(defaultImage);

			ConnectionRequest request = new ConnectionRequest(userInfo.getPresignedGetUrl(), false) {
				public void postResponse() {
					EncodedImage qr = EncodedImage.create(getResponseData());
					if (EncodedImage.create(getResponseData()).getWidth() > 5) {
						qr.scale(Math.round(Display.getInstance().getDisplayWidth() / 4),
								Math.round(Display.getInstance().getDisplayHeight() / 5));
						contactImage.setIcon(qr);
					}
					revalidate();
				}
			};
			NetworkManager.getInstance().addToQueue(request);
		} catch (Exception e) {
			System.out.println(e);
		}
		contactImage.addPointerReleasedListener(action -> {
			Image img = contactImage.getIcon();
			ImageForm imgForm = new ImageForm(userInfo.getFirstName() + " " + userInfo.getLastName(), img, this,
					logoutScreen);
			imgForm.show();
		});
		
		Container allFieldsContainer = new Container(BoxLayout.y());
		allFieldsContainer.add(contactImage);

		Container leftContainer = new Container(BoxLayout.y());
		Container rightContainer = new Container(BoxLayout.y());
		leftContainer.add(new Label("Name:","UserProfileLabel"));
		rightContainer.add(nameLabel);
		leftContainer.add(new Label("Phone:","UserProfileLabel"));
		rightContainer.add(phoneLabel);
		if (userInfo.getEmail().trim().length() > 0) {
			leftContainer.add(new Label("Email:","UserProfileLabel"));
			rightContainer.add(emailLabel);
		}
		leftContainer.add(new Label("Address:","UserProfileLabel"));
		rightContainer.add(addressLabel);
		leftContainer.add(new Label(" ","UserProfileLabel"));
		rightContainer.add(cityStZipLabel);

		Container fieldsContainer = new Container(BoxLayout.x());
		fieldsContainer.add(leftContainer);
		fieldsContainer.add(rightContainer);
		allFieldsContainer.add(fieldsContainer);
		allFieldsContainer.add(new Label(" ", "UserProfileLabel"));
		allFieldsContainer.add(showStatesButton);
		allFieldsContainer.add(new Label("Need to update personal information?", "UserProfileCenteredLabel"));
		allFieldsContainer.add(new Label("Email info@acetheevent.com", "UserProfileCenteredLabel"));
		this.add(BorderLayout.NORTH, allFieldsContainer);
	}
}
