package com.AceTheEvent.AceAppLLC;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class UserInfo {

	private double id = -1;
	private String userName = "";
	private String email = "";
	private String firstName = "";
	private String lastName = "";
	private String city = "";
	private String state = "";
	private String zip = "";
	private String address = "";
	private String phone = "";
	private String status = "";
	private String accessLevel = "";
	private String ossStatus = "";
	private String osmStatus = "";
	private String aosmStatus = "";
	private String backupStatus = "";
	private String osmtStatus = "";
	private String spotterStatus = "";
	private String scriberStatus = "";
	private String isSuspended = "";
	private String authToken = "";
	private String contactPhotoKey = "";
	private String presignedPutUrlPNG = "";
	private String presignedPutUrlJPEG = "";
	private String presignedGetUrl = "";
	private ArrayList<LinkedHashMap> states = new ArrayList();

	public UserInfo(Map<String, Object> data) {
		setInfo(data);
	}

	private void setInfo(Map<String, Object> data) {
		for (Map.Entry<String, Object> entry : data.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();
			switch (key) {
			case "id":
				id = (double) value;
				break;
			case "username":
				userName = (String) value;
				break;
			case "email":
				email = (String) value;
				break;
			case "firstName":
				firstName = (String) value;
				break;
			case "lastName":
				lastName = (String) value;
				break;
			case "city":
				city = (String) value;
				break;
			case "state":
				state = (String) value;
				break;
			case "zip":
				zip = (String) value;
				break;
			case "address":
				address = (String) value;
				break;
			case "phone":
				phone = (String) value;
				break;
			case "status":
				status = (String) value;
				break;
			case "ossStatus":
				ossStatus = (String) value;
				break;
			case "osmStatus":
				osmStatus = (String) value;
				break;
			case "aosmStatus":
				aosmStatus = (String) value;
				break;
			case "backupStatus":
				backupStatus = (String) value;
				break;
			case "osmtStatus":
				osmtStatus = (String) value;
				break;
			case "isSuspended":
				isSuspended = (String) value;
				break;
			case "isSpotter":
				spotterStatus = (String) value;
				break;
			case "isScriber":
				scriberStatus = (String) value;
				break;
			case "accessLevel":
				accessLevel = (String) value;
				break;
			case "states":
				states = (ArrayList<LinkedHashMap>) value;
				break;
			case "authToken":
				authToken = (String) value;
				break;
			case "contactPhotoKey":
				contactPhotoKey = (String) value;
				break;
			case "presignedPutUrlJPEG":
				presignedPutUrlJPEG = (String) value;
				break;
			case "presignedPutUrlPNG":
				presignedPutUrlPNG = (String) value;
				break;
			case "presignedGetUrl":
				presignedGetUrl = (String) value;
				break;
			default:
				System.out.println("This was a bad key: " + key);
				break;
			}
		}
	}

	public int getId() {
		return (int) id;
	}

	public String getUserName() {
		return userName;
	}

	public String getEmail() {
		return email;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public String getZip() {
		return zip;
	}

	public String getAddress() {
		return address;
	}

	public String getPhone() {
		return phone;
	}

	public String getStatus() {
		return status;
	}

	public String getOssStatus() {
		return ossStatus;
	}

	public String getOsmStatus() {
		return osmStatus;
	}

	public String getAosmStatus() {
		return aosmStatus;
	}

	public String getBackupStatus() {
		return backupStatus;
	}

	public String getOsmtStatus() {
		return osmtStatus;
	}

	public String getIsSuspendedStatus() {
		return isSuspended;
	}
	
	public String getSpotterStatus() {
		return spotterStatus;
	}
	
	public String getScriberStatus() {
		return scriberStatus;
	}

	public String getAuthToken() {
		return authToken;
	}

	public String getContactPhotoKey() {
		return contactPhotoKey;
	}

	public String getAccessLevel() {
		return accessLevel;
	}

	public String getPresignedPutUrlPNG() {
		return presignedPutUrlPNG;
	}

	public String getPresignedPutUrlJPEG() {
		return presignedPutUrlJPEG;
	}
	
	public String getPresignedGetUrl() {
		return presignedGetUrl;
	}

	public ArrayList<LinkedHashMap> getStates() {
		return states;
	}
}
