package com.AceTheEvent.AceAppLLC;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.codename1.components.FloatingActionButton;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.FontImage;
import com.codename1.ui.Button;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import com.codename1.ui.TextArea;

public class UserNotificationsForm extends Form {

	private SideMenuButton menuButton = null;
	private Form logoutScreen = null;
	private Form backForm = null;
	private boolean loadedFab = false;

	public UserNotificationsForm(Form backForm, ArrayList notificationsList, Form logoutScreen) {
		super("Notifications", new BorderLayout());

		this.setUIID("EventContainer");

		FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_ARROW_BACK);
//		fab.addActionListener(e -> {
//			backForm.show();
//		});
//		fab.setUIID("Fab");
//		fab.bindFabToContainer(this.getContentPane(), Component.LEFT, Component.BOTTOM);

		this.logoutScreen = logoutScreen;
		this.backForm = backForm;

		this.setUIID("backgroundWithImageNEW");

		menuButton = new SideMenuButton(this, backForm, logoutScreen, fab, true);

		loadNotifications(notificationsList);
	}

	private void loadNotifications(ArrayList notificationsList) {
		Container fields = new Container(BoxLayout.y());
		fields.setScrollableY(true);
		for (int i = 0; i < notificationsList.size(); i++) {
			HashMap<String, Object> currNotification = (HashMap<String, Object>) notificationsList.get(i);

			Container singleNotificationContainer = new Container(new BorderLayout());
			singleNotificationContainer.setUIID("EventMultiButton");
			TextArea text = new TextArea(currNotification.get("message") + "");
			text.setUIID("UserNotificationLabel");
			text.setEditable(false);  // Disable editing
			text.setFocusable(false); // Prevent focus so it doesn't look like an input
			text.addPointerReleasedListener(action -> {
				if ((currNotification.get("notificationDetails") + "").trim().length() > 0) {
					menuButton.getSpecificEvent("" + currNotification.get("notificationDetails"));
					menuButton.setLastLoaded("allEvents");
				}
			});
//			Label notificationLabel = new Label(
//					" " + currNotification.get("message") + "                                    ", "UserNotificationLabel"); // was
//			// notificationLabel
//			notificationLabel.addPointerReleasedListener(action -> {
//				if ((currNotification.get("notificationDetails") + "").trim().length() > 0) {
////						backForm.show();
//					menuButton.getSpecificEvent("" + currNotification.get("notificationDetails"));
//					menuButton.setLastLoaded("allEvents");
//				}
//			});
//				Button notificationButton = new Button("Delete");
//				notificationButton.setUIID("FatButton");
			Label deleteNotificationLabelButton = new Label("");
			deleteNotificationLabelButton.addPointerReleasedListener(event -> {
				deleteNotification((Double) currNotification.get("id"));
				if (fields.getComponentCount() == 1) {
					backForm.show();
				} else {
					UserNotificationsForm plz = new UserNotificationsForm(backForm, getUsersNotifications(),
							logoutScreen);
					plz.show();
//					fields.revalidate();
				}
			});

			Image deleteNotificationLabelButtonIcon = FontImage.createMaterial(FontImage.MATERIAL_REMOVE_CIRCLE,
					"UserNotificationLabel", 8);
			deleteNotificationLabelButton.setIcon(deleteNotificationLabelButtonIcon);

			text.setHeight(deleteNotificationLabelButton.getHeight());
//			notificationLabel.setHeight(deleteNotificationLabelButton.getHeight());

			Button b = new Button("  DELETE ");
			b.setTickerEnabled(false);
			b.setUIID("UserNotificationDeleteButton");
			b.addActionListener(event -> {
				deleteNotification((Double) currNotification.get("id"));
				if (fields.getComponentCount() == 1) {
					backForm.show();
				} else {
					UserNotificationsForm plz = new UserNotificationsForm(backForm, getUsersNotifications(),
							logoutScreen);
					plz.show();
				}
			});

			singleNotificationContainer.add(BorderLayout.WEST, text);
//			singleNotificationContainer.add(BorderLayout.WEST, notificationLabel);
//			singleNotificationContainer.add(BorderLayout.EAST, deleteNotificationLabelButton);
			singleNotificationContainer.add(BorderLayout.EAST, b);

			fields.add(singleNotificationContainer);
		}
		this.add(BorderLayout.CENTER, fields);
	}

	@Override
	public void show() {
		super.show();
		try {
			if (!loadedFab) {
				FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_ARROW_BACK);
				fab.addActionListener(e -> {
					backForm.show();
					fab.unbind();
				});
				fab.setUIID("Fab");
				fab.bindFabToContainer(this.getContentPane(), Component.LEFT, Component.BOTTOM);
				loadedFab = true;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	private void deleteNotification(Double id) {
		BackendCall deleteNotificationReq = new BackendCall("deleteNotification", true);
		HashMap<String, Object> args = new HashMap();
		args.put("notificationId", id.intValue());
		deleteNotificationReq.makeCall(args);
	}

	public ArrayList getUsersNotifications() {
		BackendCall usersNotificationsReq = new BackendCall("getNotificationsById", false);
		HashMap<String, Object> args = new HashMap();
		args.put("userId", SystemInfo.userInfo.getId());
		Map<String, Object> result = usersNotificationsReq.makeCall(args);

		return (ArrayList) result.get("root");
	}
}