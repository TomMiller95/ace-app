package com.AceTheEvent.AceAppLLC;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.codename1.components.FloatingActionButton;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.CheckBox;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.SwipeableContainer;
import com.codename1.ui.URLImage;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.SwipeBackSupport;

public class UserStatesForm extends Form {

	private boolean madeChange = false;
	private ArrayList<String> interestedStates = new ArrayList();
	private UserInfo userInfo = null;
	private SideMenuButton menuButton = null;
	
	public UserStatesForm(Form backForm, Form logoutScreen) {
		super("My Profile", new BorderLayout());
		
		this.setUIID("blueBackground");

		FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_ARROW_BACK);
		fab.addActionListener(e -> {
			backForm.show();
		});
		fab.setUIID("Fab");
		fab.bindFabToContainer(this.getContentPane(), Component.LEFT, Component.BOTTOM);

		menuButton = new SideMenuButton(this, backForm, logoutScreen, fab, true);
		
		Container titleContainer = new Container(BoxLayout.y());
		Label statesLabel = new Label("States that you want to see events for", "WhiteLabelLightBackground");
		titleContainer.add(statesLabel);
		this.add(BorderLayout.NORTH, titleContainer);
		
		Container scrollableContainer = new Container(BoxLayout.y());
		scrollableContainer.setScrollableY(true);

		userInfo = SystemInfo.userInfo;
		for (String state : getStates()) {
			Label stateField = new Label(state, "WhiteLabelLightBackground");
			for (LinkedHashMap stateInfo : userInfo.getStates()) {
				if (stateInfo.get("name").equals(state)) {
					stateField.setUIID("GoldLabelLightBackground");
					interestedStates.add(stateField.getText());
				}
			}
			stateField.addPointerReleasedListener(action -> {
				madeChange = true;
				if (stateField.getUIID().equals("WhiteLabelLightBackground")) {
					stateField.setUIID("GoldLabelLightBackground");
					interestedStates.add(stateField.getText());
				} else {
					stateField.setUIID("WhiteLabelLightBackground");
					interestedStates.remove(stateField.getText());
				}
				this.revalidate();
			});
			scrollableContainer.add(stateField);
		}
		scrollableContainer.add(new Label(" ","WhiteLabelLightBackground"));
		
		this.add(BorderLayout.CENTER, scrollableContainer);

		Button saveButton = new Button("Save Selection");
		saveButton.setTickerEnabled(false);
		saveButton.setUIID("FatButtonLight");
		saveButton.addActionListener(event -> {
			if (madeChange) {
				Dialog dialog = new Dialog("Save changes?");
				dialog.setUIID("UserStatesBackground");
				dialog.setDialogUIID("UserStatesBackground");
				dialog.getTitleComponent().setUIID("GoldLabelLightBackground");

				Label yesButtonLabel = new Label("Yes", "UserStatesBackground");
				Label noButtonLabel = new Label("No", "UserStatesBackground");

				Label noFieldsPickedLabel = new Label("PLEASE SELECT A STATUS BEFORE HITTING SUBMIT.");// TODO Pretty
																										// sure this is
																										// whats keeping
																										// the buttons
																										// centered
																										// currently...
				noFieldsPickedLabel.setVisible(false);

				Container yesNoContainer = new Container(BoxLayout.y());
				yesNoContainer.setUIID("UserStatesBackground");
				yesNoContainer.add(yesButtonLabel);
				yesNoContainer.add(noButtonLabel);
				yesNoContainer.add(new Label(" ", "UserStatesBackground"));
				yesNoContainer.add(noFieldsPickedLabel);

				yesButtonLabel.addPointerReleasedListener(action -> {
					dialog.dispose();
					saveInfo();
				});
				noButtonLabel.addPointerReleasedListener(action -> {
					dialog.dispose();
					backForm.show();
				});
				dialog.add(yesNoContainer);
				dialog.setDisposeWhenPointerOutOfBounds(false);
				dialog.show();
			} else {
				backForm.show();
			}
		});
		this.add(BorderLayout.SOUTH, saveButton);
	}
	
	private void saveInfo() {
		BackendCall loginReq = new BackendCall("setInterestedStates", true);
		HashMap<String, Object> args = new HashMap();
		args.put("states", interestedStates);
		args.put("userId", userInfo.getId());
		args.put("presignedGetUrl", userInfo.getPresignedGetUrl());
		Map<String, Object> result = loginReq.makeCall(args);
		userInfo = new UserInfo(result);
		SystemInfo.userInfo = userInfo;
		menuButton.getAllEvents();
		EventsForm eventsForm = new EventsForm(this);
		eventsForm.setLastLookUp("allEvents");
		eventsForm.show();
	}

	private String[] getAbbreviations() {
		String[] abbrevs = { "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "HI", "ID", "IL", "IN",
				"IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM",
				"NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV",
				"WI", "WY" };
		return abbrevs;
	}

	private String[] getStates() {
		String[] abbrevs = { "Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut",
				"Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas",
				"Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi",
				"Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York",
				"North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island",
				"South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington",
				"Washington D.C.", "West Virginia", "Wisconsin", "Wyoming" };
		return abbrevs;
	}
}
